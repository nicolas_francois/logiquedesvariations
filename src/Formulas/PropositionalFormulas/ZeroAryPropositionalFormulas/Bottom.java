package Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas;

import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.BottomVar;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.TopVar;
import TruthTables.AssociationTable;

public class Bottom extends Constant {

    public Bottom() { super(false); }

    public PropositionalFormula toNFNFneg() { return new Top(); }

    public VariationalFormula toNFNFeqf() { return new TopVar(new VariationSymbol(Symbol.EQUALSTRUE)); }
    public VariationalFormula toNFNFeqt() { return new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); }
    public VariationalFormula toNFNFpls() { return new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); }
    public VariationalFormula toNFNFmns() { return new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); }

    public PropositionalDNFFormula toDNF() { return null; }
    public PropositionalDNFFormula toDNFneg() { return new PropositionalDNFFormula(); }

    @Override
    public Boolean evaluateInitial(AssociationTable at) { return false; }

    @Override
    public Boolean evaluateFinal(AssociationTable at) { return false; }
}
