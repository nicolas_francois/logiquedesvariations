package Formulas.VariationalFormulas.PropositionalVariations;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryConjunctionVar;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.VarPropositionalFormulas.VarPropFormula;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

import java.util.HashSet;

/**
 * Created by Nicolas FRANCOIS on 23/04/2023.
 */

public class PropositionalVariation extends VariationalFormula {

    PropositionalFormula prop1;
    PropositionalFormula prop2;

    public PropositionalVariation(PropositionalFormula p1, PropositionalFormula p2) {
        prop1 = p1;
        prop2 = p2;
    }

    public HashSet<VariationalFormula> getPropSet() {
        // Irrelevant for this case, tableaux works with negative normal form
        return null;
    }

    @Override
    public HashSet<String> getVariables() {
        HashSet<String> bvfvars = new HashSet<>();
        for (String v: prop1.getVariables()) { bvfvars.add(v); }
        for (String v: prop2.getVariables()) { bvfvars.add(v); }
        return bvfvars;
    }

    @Override
    public VariationalFormula toNFNF() {
        NAryDisjunctionVar prop1vag = new NAryDisjunctionVar();
        prop1vag.addProposition(prop1.toNFNFeqt());
        prop1vag.addProposition(prop1.toNFNFmns());
        NAryDisjunctionVar prop2vad = new NAryDisjunctionVar();
        prop2vad.addProposition(prop2.toNFNFeqt());
        prop2vad.addProposition(prop2.toNFNFpls());
        NAryConjunctionVar nacv = new NAryConjunctionVar();
        nacv.addProposition(prop1vag);
        nacv.addProposition(prop2vad);
        return nacv;
    }

    @Override
    public VariationalFormula toNFNFneg() {
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        VariationalFormula prop1eqf = new VarPropFormula(prop1,new VariationSymbol(Symbol.EQUALSFALSE));
        VariationalFormula prop1pls = new VarPropFormula(prop1,new VariationSymbol(Symbol.PLUS));
        VariationalFormula prop2eqf = new VarPropFormula(prop2,new VariationSymbol(Symbol.EQUALSFALSE));
        VariationalFormula prop2mns = new VarPropFormula(prop2,new VariationSymbol(Symbol.MINUS));
        nadv.addProposition(prop1eqf.toNFNF());
        nadv.addProposition(prop1pls.toNFNF());
        nadv.addProposition(prop2eqf.toNFNFneg());
        nadv.addProposition(prop2mns.toNFNFneg());
        return nadv;
    }

    @Override
    public Boolean evaluate(AssociationTable at) {
        return prop1.evaluateInitial(at) && prop2.evaluateFinal(at);
    }

    public DNFFormula toDNF() { return null; }

    public DNFFormula toDNFneg() { return null; }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(prop1.toString());
        res.append(" |> ");
        res.append(prop2.toString());
        return res.toString();
    }
}
