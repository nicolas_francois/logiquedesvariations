package Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms;

import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryConjunctionVar;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.TopVar;
import TruthTables.AssociationTable;

import java.util.HashSet;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class DNFFormula extends VariationalFormula {

    // The list of cubes of the disjunction.
    // An empty list represents bottom
    // A null DNFFormula represents top
    protected CubesList cubes;

    public DNFFormula() { cubes = new CubesList(); }

    public DNFFormula(CubesList cl) { cubes = cl; }

    public void addCube(Cube c) { cubes.addCube(c); }

    public void addCubesList(CubesList cl) { for (Cube c : cl) { cubes.addCube(c); } }

    public CubesList getCubes() { return cubes; }

    @Override
    public String toString() { return cubes.toString(); }

    @Override
    public HashSet<VariationalFormula> getPropSet() {
        return null;
    }

    @Override
    public HashSet<String> getVariables() {
        return null;
    }

    @Override
    public VariationalFormula toNFNF() {
        return this;
    }

    @Override
    public VariationalFormula toNFNFneg() {
        if (cubes.getSize() == 0) { return new TopVar(new VariationSymbol(Symbol.EQUALSTRUE)); }
        NAryConjunctionVar cv = new NAryConjunctionVar();
        for (Cube c: cubes) {
            NAryDisjunctionVar dv = new NAryDisjunctionVar();
            for (VariableVariation vv: c) {
                dv.addProposition(vv.toNeg());
            }
            cv.addProposition(dv);
        }
        return cv;
    }

    public VariationalFormula toFragment() {
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        for (Cube c: this.getCubes()) { nadv.addProposition(c.toFragment()); }
        return nadv;
    }

    @Override
    public Boolean evaluate(AssociationTable at) { return cubes.evaluate(at); }

    @Override
    public DNFFormula toDNF() {
        return null;
    }
}
