package Formulas.VariationalFormulas.VariationSymbols;

import java.util.Objects;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class VariationSymbol {

    private final Symbol symb;

    public VariationSymbol(Symbol s) { symb = s; }

    public Symbol getSymbol() { return symb; }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        switch (symb) {
            case EQUALSTRUE:
                res.append("=v");
                break;
            case EQUALSFALSE:
                res.append("=f");
                break;
            case PLUS:
                res.append("+");
                break;
            case MINUS:
                res.append("-");
                break;
            case DIFF:
                res.append("!=");
                break;
            case EQUALS:
                res.append("=");
                break;
            case LEFTTRUE:
                res.append("v.");
                break;
            case LEFTFALSE:
                res.append("f.");
                break;
            case RIGHTTRUE:
                res.append(".v");
                break;
            case RIGHTFALSE:
                res.append(".f");
                break;
        }
        return res.toString();
    }

    // Compares the symbol with a primary symbol s
    public Boolean match(Symbol s) {
        switch (symb) {
            case EQUALSTRUE:
                return s.equals(Symbol.EQUALSTRUE);
            case EQUALSFALSE:
                return s.equals(Symbol.EQUALSFALSE);
            case PLUS:
                return s.equals(Symbol.PLUS);
            case MINUS:
                return s.equals(Symbol.MINUS);
            case DIFF:
                return (s.equals(Symbol.PLUS) || s.equals(Symbol.MINUS));
            case EQUALS:
                return (s.equals(Symbol.EQUALSTRUE) || s.equals(Symbol.EQUALSFALSE));
            case LEFTTRUE:
                return (s.equals(Symbol.EQUALSTRUE) || s.equals(Symbol.MINUS));
            case LEFTFALSE:
                return (s.equals(Symbol.EQUALSFALSE) || s.equals(Symbol.PLUS));
            case RIGHTTRUE:
                return (s.equals(Symbol.EQUALSTRUE) || s.equals(Symbol.PLUS));
            default: // RIGHTFALSE:
                return (s.equals(Symbol.EQUALSFALSE) || s.equals(Symbol.MINUS));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VariationSymbol that = (VariationSymbol) o;
        // System.out.println("Comparaison de "+symb+" et "+that.symb+" : "+(symb == that.symb));
        return symb == that.symb;
    }

    @Override
    public int hashCode() { return Objects.hash(symb); }
}
