package Formulas.PropositionalFormulas.BinAryPropositionalFormulas;

import Formulas.PropositionalFormulas.NAryPropositionalFormulas.NAryConjunction;
import Formulas.PropositionalFormulas.NAryPropositionalFormulas.NAryDisjunction;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.PropositionalFormulas.UnAryPropositionalFormulas.Negation;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryConjunctionVar;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.VarPropositionalFormulas.VarPropFormula;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

public class Disjunction extends BinAryFormula {

    public Disjunction(PropositionalFormula p1, PropositionalFormula p2) {
        super(p1, p2);
    }

    @Override
    public PropositionalFormula toNFNF() {
        // System.out.println("PropBinDisNNF : "+prop1.toString()+","+prop2.toString());
        return new Disjunction(prop1.toNFNF(), prop2.toNFNF());
    }

    @Override
    public PropositionalFormula toNFNFneg() {
        // System.out.println("PropBinDisNNFneg : "+prop1.toString()+","+prop2.toString());
        return new Conjunction(prop1.toNFNFneg(), prop2.toNFNFneg());
    }

    public VariationalFormula toNFNFeqf() {
        // System.out.println("PropBinDisNNFeqf : "+prop1.toString()+","+prop2.toString());
        NAryConjunctionVar nacj = new NAryConjunctionVar();
        VariationalFormula prop1eqt = (new VarPropFormula(prop1, new VariationSymbol(Symbol.EQUALSFALSE))).toNFNF();
        VariationalFormula prop2eqt = (new VarPropFormula(prop2, new VariationSymbol(Symbol.EQUALSFALSE))).toNFNF();
        nacj.addProposition(prop1eqt);
        nacj.addProposition(prop2eqt);
        return nacj;
    }

    public VariationalFormula toNFNFeqt() {
        // System.out.println("PropBinDisNNFeqt : "+prop1.toString()+","+prop2.toString());
        NAryDisjunctionVar nadj = new NAryDisjunctionVar();
        VariationalFormula prop1eqt = (new VarPropFormula(prop1, new VariationSymbol(Symbol.EQUALSTRUE))).toNFNF();
        VariationalFormula prop1plus = (new VarPropFormula(prop1, new VariationSymbol(Symbol.PLUS))).toNFNF();
        VariationalFormula prop1minus = (new VarPropFormula(prop1, new VariationSymbol(Symbol.MINUS))).toNFNF();
        VariationalFormula prop2eqt = (new VarPropFormula(prop2, new VariationSymbol(Symbol.EQUALSTRUE))).toNFNF();
        VariationalFormula prop2plus = (new VarPropFormula(prop2, new VariationSymbol(Symbol.PLUS))).toNFNF();
        VariationalFormula prop2minus = (new VarPropFormula(prop2, new VariationSymbol(Symbol.MINUS))).toNFNF();
        NAryConjunctionVar p1plusp2minus = new NAryConjunctionVar();
        p1plusp2minus.addProposition(prop1plus);
        p1plusp2minus.addProposition(prop2minus);
        NAryConjunctionVar p1minusp2plus = new NAryConjunctionVar();
        p1minusp2plus.addProposition(prop1minus);
        p1minusp2plus.addProposition(prop2plus);
        nadj.addProposition(prop1eqt);
        nadj.addProposition(p1plusp2minus);
        nadj.addProposition(p1minusp2plus);
        nadj.addProposition(prop2eqt);
        return nadj;
    }

    public VariationalFormula toNFNFpls() {
        // System.out.println("PropBinDisNNFpls : "+prop1.toString()+","+prop2.toString());
        NAryDisjunctionVar nadj = new NAryDisjunctionVar();
        VariationalFormula prop1eqf = (new VarPropFormula(prop1, new VariationSymbol(Symbol.EQUALSFALSE))).toNFNF();
        VariationalFormula prop1plus = (new VarPropFormula(prop1, new VariationSymbol(Symbol.PLUS))).toNFNF();
        VariationalFormula prop2eqf = (new VarPropFormula(prop2, new VariationSymbol(Symbol.EQUALSFALSE))).toNFNF();
        VariationalFormula prop2plus = (new VarPropFormula(prop2, new VariationSymbol(Symbol.PLUS))).toNFNF();
        NAryConjunctionVar p1plusp2plus = new NAryConjunctionVar();
        p1plusp2plus.addProposition(prop1plus);
        p1plusp2plus.addProposition(prop2plus);
        NAryConjunctionVar p1plusp2eqf = new NAryConjunctionVar();
        p1plusp2eqf.addProposition(prop1plus);
        p1plusp2eqf.addProposition(prop2eqf);
        NAryConjunctionVar p1eqfp2plus = new NAryConjunctionVar();
        p1eqfp2plus.addProposition(prop1eqf);
        p1eqfp2plus.addProposition(prop2plus);
        nadj.addProposition(p1plusp2plus);
        nadj.addProposition(p1plusp2eqf);
        nadj.addProposition(p1eqfp2plus);
        return nadj;
    }

    public VariationalFormula toNFNFmns() {
        // System.out.println("PropBinDisNNFmns : "+prop1.toString()+","+prop2.toString());
        NAryDisjunctionVar nadj = new NAryDisjunctionVar();
        VariationalFormula prop1eqf = (new VarPropFormula(prop1, new VariationSymbol(Symbol.EQUALSFALSE))).toNFNF();
        VariationalFormula prop1minus = (new VarPropFormula(prop1, new VariationSymbol(Symbol.MINUS))).toNFNF();
        VariationalFormula prop2eqf = (new VarPropFormula(prop2, new VariationSymbol(Symbol.EQUALSFALSE))).toNFNF();
        VariationalFormula prop2minus = (new VarPropFormula(prop2, new VariationSymbol(Symbol.MINUS))).toNFNF();
        NAryConjunctionVar p1minusp2minus = new NAryConjunctionVar();
        p1minusp2minus.addProposition(prop1minus);
        p1minusp2minus.addProposition(prop2minus);
        NAryConjunctionVar p1minusp2eqf = new NAryConjunctionVar();
        p1minusp2eqf.addProposition(prop1minus);
        p1minusp2eqf.addProposition(prop2eqf);
        NAryConjunctionVar p1eqfp2minus = new NAryConjunctionVar();
        p1eqfp2minus.addProposition(prop1eqf);
        p1eqfp2minus.addProposition(prop2minus);
        nadj.addProposition(p1minusp2minus);
        nadj.addProposition(p1minusp2eqf);
        nadj.addProposition(p1eqfp2minus);
        return nadj;
    }

    @Override
    public PropositionalDNFFormula toDNF() {
        NAryDisjunction nad = new NAryDisjunction();
        nad.addProposition(prop1);
        nad.addProposition(prop2);
        return nad.toDNF();
    }

    @Override
    public PropositionalDNFFormula toDNFneg() {
        NAryConjunction nac = new NAryConjunction();
        nac.addProposition(new Negation(prop1));
        nac.addProposition(new Negation(prop2));
        return nac.toDNF();
    }

    @Override
    public Boolean evaluateInitial(AssociationTable at) { return prop1.evaluateInitial(at) || prop2.evaluateInitial(at); }

    @Override
    public Boolean evaluateFinal(AssociationTable at) { return prop1.evaluateFinal(at) || prop2.evaluateFinal(at); }

    @Override
    public String toString() {
        return super.toString("OR");
    }
}
