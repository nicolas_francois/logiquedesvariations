package Formulas.VariationalFormulas.ZeroAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;

import java.util.HashSet;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public abstract class ZeroAryVarFormula extends VariationalFormula {

    protected VariationSymbol vs;

    public ZeroAryVarFormula(VariationSymbol v) { vs = v; }

    public VariationSymbol getVS() { return vs; }

    @Override
    public HashSet<VariationalFormula> getPropSet() {
        HashSet<VariationalFormula> hspf = new HashSet<>();
        hspf.add(this);
        return hspf;
    }
}
