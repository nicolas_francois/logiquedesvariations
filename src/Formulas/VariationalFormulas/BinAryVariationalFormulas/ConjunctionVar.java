package Formulas.VariationalFormulas.BinAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryConjunctionVar;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.UnAryVariationalFormulas.NegationVar;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class ConjunctionVar extends BinAryVarFormula {

    public ConjunctionVar(VariationalFormula p1, VariationalFormula p2) { super(p1, p2); }

    @Override
    public Boolean isConjunction() { return true; }

    @Override
    public VariationalFormula toNFNF() { return new ConjunctionVar(prop1.toNFNF(), prop2.toNFNF()); }

    @Override
    public VariationalFormula toNFNFneg() {return new DisjunctionVar(prop1.toNFNFneg(), prop2.toNFNFneg()); }

    @Override
    public Boolean evaluate(AssociationTable at) { return prop1.evaluate(at) && prop2.evaluate(at); }

    public DNFFormula toDNF() {
        NAryConjunctionVar nacv = new NAryConjunctionVar();
        nacv.addProposition(prop1);
        nacv.addProposition(prop2);
        return nacv.toDNF();
    }

    public DNFFormula toDNFneg() {
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        nadv.addProposition(new NegationVar(prop1));
        nadv.addProposition(new NegationVar(prop2));
        return nadv.toDNF();
    }

    @Override
    public String toString() {
        return super.toString("AND");
    }
}
