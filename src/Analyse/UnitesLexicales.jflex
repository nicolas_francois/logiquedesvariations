package Analyse;

import java_cup.runtime.*;
import Exceptions.AnalyseLexicaleException;

%%

%class AnalyseurLexical
%public

%line
%column

%type Symbol
%eofval{
        return symbol(CodesLexicaux.EOF) ;
%eofval}

%cup

%{

  private StringBuilder chaine ;

  private Symbol symbol(int type) {
	return new Symbol(type, yyline, yycolumn) ;
  }

  private Symbol symbol(int type, Object value) {
	return new Symbol(type, yyline, yycolumn, value) ;
  }
%}

chiffre = [0-9]
lettre = [a-zA-Z]
chiffrelettre = {chiffre} | {lettre}
csteE = {chiffre}+
booleen = "vrai" | "faux"
finDeLigne = \r|\n
espace = {finDeLigne}  | [ \t\f]
idf = {lettre}{chiffrelettre}*

%%
"//".*                                    { /* DO NOTHING */ }

";"                    { return symbol(CodesLexicaux.SEMICOLON); }
"non"                  { return symbol(CodesLexicaux.NOT); }
"et"                   { return symbol(CodesLexicaux.AND); }
"oux"                  { return symbol(CodesLexicaux.XOR); }
"ou"                   { return symbol(CodesLexicaux.OR); }
"=>"                   { return symbol(CodesLexicaux.IMPLIES); }
"<=>"                  { return symbol(CodesLexicaux.IIF); }
"("                    { return symbol(CodesLexicaux.LPAREN); }
")"                    { return symbol(CodesLexicaux.RPAREN); }
"^"                    { return symbol(CodesLexicaux.TOTHE); }
"=v"                   { return symbol(CodesLexicaux.EQUALSTRUE); }
"=f"                   { return symbol(CodesLexicaux.EQUALSFALSE); }
"+"                    { return symbol(CodesLexicaux.PLUS); }
"-"                    { return symbol(CodesLexicaux.MINUS); }
"!="                   { return symbol(CodesLexicaux.DIFF); }
"=="                   { return symbol(CodesLexicaux.EQUALS); }
"v."                   { return symbol(CodesLexicaux.LEFTTRUE); }
".v"                   { return symbol(CodesLexicaux.RIGHTTRUE); }
"f."                   { return symbol(CodesLexicaux.LEFTFALSE); }
".f"                   { return symbol(CodesLexicaux.RIGHTFALSE); }
// "-->"                  { return symbol(CodesLexicaux.CONS); }
"|>"                   { return symbol(CodesLexicaux.VARIATION); }
{idf}                  { return symbol(CodesLexicaux.IDENTIFIER, yytext()); }

{espace}               { }
