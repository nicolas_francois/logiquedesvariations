package Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms;

import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.Atom;
import TruthTables.AssociationTable;

import static Formulas.VariationalFormulas.VariationSymbols.Symbol.*;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class VariableVariation {

    private String var;
    private Boolean signBefore;
    private Boolean signAfter;

    public VariableVariation(String name, Boolean sb, Boolean sa) {
        var = name;
        signBefore = sb;
        signAfter = sa;
    }

    public String getVariable() { return var; }

    public Boolean getSignBefore() { return signBefore; }

    public Boolean getSignAfter() { return signAfter; }

    public VariationalFormula toNeg() {
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        if (signBefore) {
            if (signAfter) {
                // nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSTRUE)));
                nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSFALSE)));
                nadv.addProposition(new Atom(var,new VariationSymbol(PLUS)));
                nadv.addProposition(new Atom(var,new VariationSymbol(MINUS)));
            }
            else {
                nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSTRUE)));
                nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSFALSE)));
                nadv.addProposition(new Atom(var,new VariationSymbol(PLUS)));
                // nadv.addProposition(new Atom(var,new VariationSymbol(MINUS)));
            }
        }
        else{
            if (signAfter) {
                nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSTRUE)));
                nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSFALSE)));
                // nadv.addProposition(new Atom(var,new VariationSymbol(PLUS)));
                nadv.addProposition(new Atom(var,new VariationSymbol(MINUS)));
            }
            else {
                nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSTRUE)));
                // nadv.addProposition(new Atom(var,new VariationSymbol(EQUALSFALSE)));
                nadv.addProposition(new Atom(var,new VariationSymbol(PLUS)));
                nadv.addProposition(new Atom(var,new VariationSymbol(MINUS)));
            }
        }
        return nadv;
    }

    public Boolean evaluate(AssociationTable at) {
        Boolean rep = false;
        VariationSymbol vs = at.getSymbol(var);
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
                rep = signBefore && signAfter;
                break;
            case EQUALSFALSE:
                rep = !signBefore && !signAfter;
                break;
            case PLUS:
                rep = !signBefore && signAfter;
                break;
            case MINUS:
                rep = signBefore && !signAfter;
                break;
            case EQUALS:
                rep = signBefore == signAfter;
                break;
            case DIFF:
                rep = signBefore != signAfter;
                break;
            case LEFTTRUE:
                rep = signBefore;
                break;
            case LEFTFALSE:
                rep = !signBefore;
                break;
            case RIGHTTRUE:
                rep = signAfter;
                break;
            case RIGHTFALSE:
                rep = !signAfter;
                break;
        }
        return rep;
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(var);
        if (signBefore) {
            if (signAfter) { res.append("^=v"); }
            else { res.append("^-"); }
        }
        else{
            if (signAfter) { res.append("^+"); }
            else { res.append("^=f"); }
        }
        return res.toString();
    }
}
