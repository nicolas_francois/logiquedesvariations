package Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms;

import java.util.ArrayList;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class DNFNAryDisj extends DNFFormula {

    private ArrayList<DNFFormula> dnfFormList;

    public DNFNAryDisj() {
        super();
        dnfFormList = new ArrayList<>();
    }

    public void addProposition(DNFFormula f) { dnfFormList.add(f); }

    public DNFFormula toDNF() {
        DNFFormula rep = new DNFFormula();
        for (DNFFormula dnff: dnfFormList) { rep.addCubesList(dnff.getCubes()); }
        return rep;
    }
}
