package Formulas.VariationalFormulas.BinAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryConjunctionVar;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.UnAryVariationalFormulas.NegationVar;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class ImplicationVar extends BinAryVarFormula {

    public ImplicationVar(VariationalFormula p1, VariationalFormula p2) { super(p1, p2); }

    @Override
    public VariationalFormula toNFNF() {
        return new DisjunctionVar(prop1.toNFNFneg(), prop2.toNFNF());
    }

    @Override
    public VariationalFormula toNFNFneg() {
        return new ConjunctionVar(prop1.toNFNF(), prop2.toNFNFneg());
    }

    @Override
    public Boolean evaluate(AssociationTable at) {
        // DEBUG
        // System.out.println("implication : "+prop1.evaluate(at)+"=>"+prop2.evaluate(at));
        // System.out.println("Proposition 1 : "+prop1.toString());
        // System.out.println("Proposition 2 : "+prop2.toString());
        // System.out.println(!prop1.evaluate(at));
        // System.out.println(prop2.evaluate(at));
        // System.out.println(!prop1.evaluate(at) || prop2.evaluate(at));
        // FIN DEBUG
        return !(prop1.evaluate(at)) || prop2.evaluate(at); }

    public DNFFormula toDNF() {
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        nadv.addProposition(new NegationVar(prop1));
        nadv.addProposition(prop2);
        return nadv.toDNF();
    }

    public DNFFormula toDNFneg() {
        NAryConjunctionVar nacv = new NAryConjunctionVar();
        nacv.addProposition(prop1);
        nacv.addProposition(new NegationVar(prop2));
        return nacv.toDNF();
    }

    @Override
    public String toString() {
        return super.toString("=>");
    }
}
