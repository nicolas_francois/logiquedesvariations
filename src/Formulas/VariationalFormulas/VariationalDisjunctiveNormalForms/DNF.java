package Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms;

import Formulas.VariationalFormulas.VariationalFormula;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class DNF {

    private VariationalFormula vf;

    private DNFFormula dnfvf;

    public DNF(VariationalFormula f) {
        vf = f;
        dnfvf = vf.toDNF();
    }

    public DNFFormula getDNF() { return dnfvf; }

    /* private ConstantVar toDNF(ConstantVar constvar) { return (ConstantVar) constvar.toNNF(); }

    private ConstantVar toDNFneg(ConstantVar constvar) { return (ConstantVar) constvar.toNNFneg(); }

    private VariableVariation toDNF(VariableVar varvar) {
        NAryDisjunctionVar res = new NAryDisjunctionVar();
        PropositionalFormula posalpha = new Variable(varvar.getName());
        PropositionalFormula negalpha = new Negation(new Variable(varvar.getName()));
        switch (varvar.getVS().getSymbol()) {
            case EQUALSTRUE: // a^=v = a |> a
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                break;
            case EQUALSFALSE: // a^=f = non a |> non a
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case PLUS: // a^+ = non a |> a
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                break;
            case MINUS: // a^- = a |> non a
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                break;
            case DIFF: // a^!= = (a |> non a) ou (non a |> a)
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                break;
            case EQUALS: // a^= = (a |> a) ou (non a |> non a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case LEFTTRUE: // a^v. = (a |> a) ou (a |> non a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                break;
            case LEFTFALSE: // a^f. = (non a |> non a) ou (non a |> a)
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                break;
            case RIGHTTRUE: // a^.v = (non a |> a) ou (a |> a)
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                break;
            case RIGHTFALSE: // a^.f = (non a |> non a) ou (a |> non a)
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                res.addProposition( new PropositionalVariation(posalpha,negalpha));
                break;
        }
        return res;
    }

    private VariableVariation toDNFneg(VariableVar varvar) {
        NAryDisjunctionVar res = new NAryDisjunctionVar();
        PropositionalFormula posalpha = new Variable(varvar.getName());
        PropositionalFormula negalpha = new Negation(new Variable(varvar.getName()));
        switch (varvar.getVS().getSymbol()) {
            case EQUALSTRUE: // non a^=v = (a |> non a) ou (non a |> a) ou (non a |> non a)
                // res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case EQUALSFALSE: // non a^=f = (a |> a) ou (non a |> a) ou (a |> non a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                // res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case PLUS: // non a^+ = (a |> a) ou (a |> non a) ou (non a |> non a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                // res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case MINUS: // non a^- = (a |> a) ou (non a |> a) ou (non a |> non a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                // res.addProposition(new PropositionalVariation(posalpha,negalpha));
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case DIFF: // non a^!= = (a |> a) ou (non a |> non a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                // res.addProposition(new PropositionalVariation(posalpha,negalpha));
                // res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case EQUALS: // non a^= = (a |> non a) ou (non a |> a)
                // res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                // res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case LEFTTRUE: // non a^v. = (non a |> non a) ou (non a |> a)
                // res.addProposition(new PropositionalVariation(posalpha,posalpha));
                // res.addProposition(new PropositionalVariation(posalpha,negalpha));
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case LEFTFALSE: // non a^f. = (a |> a) ou (a |> non a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                // res.addProposition(new PropositionalVariation(negalpha,posalpha));
                // res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case RIGHTTRUE: // non a^.v = (non a |> non a) ou (a |> non a)
                // res.addProposition(new PropositionalVariation(posalpha,posalpha));
                res.addProposition(new PropositionalVariation(posalpha,negalpha));
                // res.addProposition(new PropositionalVariation(negalpha,posalpha));
                res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
            case RIGHTFALSE: // non a^.f = (non a |> a) ou (a |> a)
                res.addProposition(new PropositionalVariation(posalpha,posalpha));
                // res.addProposition(new PropositionalVariation(posalpha,negalpha));
                res.addProposition(new PropositionalVariation(negalpha,posalpha));
                // res.addProposition(new PropositionalVariation(negalpha,negalpha));
                break;
        }
        return res;
    }

    public VariationalFormula toDNF(NegationVar negvar) { return toDNFneg(negvar.getProp()); }

    public VariationalFormula toDNFneg(NegationVar negvar) { return toDNF(negvar.getProp()); }

    public VariationalFormula toDNF(ConjunctionVar conjvar) {
        NAryConjunctionVar nacv = new NAryConjunctionVar();
        nacv.addProposition(conjvar.getProp1());
        nacv.addProposition(conjvar.getProp2());
        return toDNF(nacv);
    }

    public VariationalFormula toDNFneg(ConjunctionVar conjvar) {
        NAryConjunctionVar nacv = new NAryConjunctionVar();
        nacv.addProposition(new NegationVar(conjvar.getProp1()));
        nacv.addProposition(new NegationVar(conjvar.getProp2()));
        return toDNF(nacv);
    } */


}
