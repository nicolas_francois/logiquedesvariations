package Formulas.VariationalFormulas.BinAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalFormula;

import java.util.HashSet;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public abstract class BinAryVarFormula extends VariationalFormula {

    protected VariationalFormula prop1;
    protected VariationalFormula prop2;

    public BinAryVarFormula(VariationalFormula p1, VariationalFormula p2) {
        prop1 = p1;
        prop2 = p2;
    }

    public HashSet<VariationalFormula> getPropSet() {
        HashSet<VariationalFormula> hspf = new HashSet<>();
        hspf.add(prop1);
        hspf.add(prop2);
        return hspf;
    }

    public VariationalFormula getProp1() { return prop1; }
    public VariationalFormula getProp2() { return prop2; }

    @Override
    public HashSet<String> getVariables() {
        HashSet<String> bvfvars = new HashSet<>();
        for (String v: prop1.getVariables()) { bvfvars.add(v); }
        for (String v: prop2.getVariables()) { bvfvars.add(v); }
        return bvfvars;
    }

    public String toString(String op) {
        StringBuilder res = new StringBuilder("(");
        res.append(prop1.toString());
        res.append(" ").append(op).append(" ");
        res.append(prop2.toString());
        res.append(")");
        return res.toString();
    }

    // public DNFFormula toDNF() { return null; }

    // public DNFFormula toDNFneg() { return null; }
}
