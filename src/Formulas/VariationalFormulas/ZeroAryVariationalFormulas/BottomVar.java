package Formulas.VariationalFormulas.ZeroAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;

import static Formulas.VariationalFormulas.VariationSymbols.Symbol.EQUALSTRUE;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class BottomVar extends ConstantVar {

    public BottomVar(VariationSymbol v) { super(v, false); }

    @Override
    public VariationalFormula toNFNF() {
        VariationalFormula rest = new TopVar(new VariationSymbol(EQUALSTRUE));
        VariationalFormula resf = new BottomVar(new VariationSymbol(EQUALSTRUE));
        VariationalFormula res = rest; // En attendant d'avoir la bonne réponse
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
            case PLUS:
            case MINUS:
            case DIFF:
            case LEFTTRUE:
            case RIGHTTRUE:
                res = resf;
                break;
            case EQUALSFALSE:
            case EQUALS:
            case LEFTFALSE:
            case RIGHTFALSE:
                res = rest;
                break;
        }
        return res;
    }

    @Override
    public VariationalFormula toNFNFneg() {
        VariationalFormula rest = new TopVar(new VariationSymbol(EQUALSTRUE));
        VariationalFormula resf = new BottomVar(new VariationSymbol(EQUALSTRUE));
        VariationalFormula res = rest; // En attendant d'avoir la bonne réponse
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
            case PLUS:
            case MINUS:
            case DIFF:
            case LEFTTRUE:
            case RIGHTTRUE:
                res = rest;
                break;
            case EQUALSFALSE:
            case EQUALS:
            case LEFTFALSE:
            case RIGHTFALSE:
                res = resf;
                break;
        }
        return res;
    }

    public DNFFormula toDNF() {
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
            case PLUS:
            case MINUS:
            case DIFF:
            case LEFTTRUE:
            case RIGHTTRUE:
                // null DNFFormula == Bottom
                return null;
            default:
                // empty DNFFormula == Top
                return new DNFFormula();
        }
    }

    @Override
    public DNFFormula toDNFneg() {
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
            case PLUS:
            case MINUS:
            case DIFF:
            case LEFTTRUE:
            case RIGHTTRUE:
                // empty DNFFormula == Top
                return new DNFFormula();
            default:
                // null DNFFormula == Bottom
                return null;
        }
    }
}
