package Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 12/05/2023.
 */

public class PropositionalCubesList implements Iterable<PropositionalCube> {

    private ArrayList<PropositionalCube> alPropositionalCubes;

    public PropositionalCubesList() { alPropositionalCubes = new ArrayList<>(); }
    public PropositionalCubesList(ArrayList<PropositionalCube> alc) { alPropositionalCubes = alc; }

    public void addCube(PropositionalCube c) { alPropositionalCubes.add(c); }

    public void addCubesList(PropositionalCubesList cl) { for (PropositionalCube c: cl) { addCube(c); }}

    public Iterator<PropositionalCube> iterator() { return alPropositionalCubes.iterator(); }

    public PropositionalCubesList timesCube(PropositionalCube c) {
        PropositionalCubesList rep = new PropositionalCubesList();
        if (c == null) { return rep; }
        PropositionalCube temp;
        for (PropositionalCube cc: alPropositionalCubes) {
            temp = cc.timesCube(c);
            if (temp != null) { rep.addCube(temp); }
        }
        return rep;
    }

    public PropositionalCubesList timesCubesList(PropositionalCubesList cl) {
        if (cl == null) { return null; }
        PropositionalCubesList rep = new PropositionalCubesList();
        PropositionalCubesList temp;
        for (PropositionalCube c: alPropositionalCubes) { rep.addCube(c); }
        if (cl == null) { return rep; }
        for (PropositionalCube c1: alPropositionalCubes) {
            temp = this.timesCube(c1);
            if (temp != null) { rep.addCubesList(temp); }
        }
        return rep;
    }
}
