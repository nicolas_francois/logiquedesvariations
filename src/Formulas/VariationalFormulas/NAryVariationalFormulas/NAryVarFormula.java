package Formulas.VariationalFormulas.NAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.Formula;
import Formulas.VariationalFormulas.VariationalFormula;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public abstract class NAryVarFormula extends VariationalFormula {

    protected HashSet<VariationalFormula> propSet;

    public NAryVarFormula() { propSet = new HashSet<>(); }

    public void addProposition(VariationalFormula p) { propSet.add(p); }

    public HashSet<VariationalFormula> getPropSet() {
        HashSet<VariationalFormula> hspf = new HashSet<>();
        for (VariationalFormula f: propSet) { hspf.add(f); }
        return hspf;
    }

    @Override
    public HashSet<String> getVariables() {
        HashSet<String> navfvars = new HashSet<>();
        for (Formula f: propSet) {
            for (String v: f.getVariables()) { navfvars.add(v); }
        }
        return navfvars;
    }

    public DNFFormula toDNF() { return null; }

    public DNFFormula toDNFneg() { return null; }

    public String toString(String op) {
        StringBuilder res = new StringBuilder();
        if (propSet.size() == 0) { return res.toString(); } // Should never occur !!!
        else { // Even case "propSet.size() == 1" should not occur
            res.append("(");
            Iterator<VariationalFormula> propIt = propSet.iterator();
            res.append(propIt.next().toString());
            while (propIt.hasNext()) {
                res.append(" ").append(op).append(" ").append(propIt.next());
            }
            res.append(")");
            return res.toString();
        }
    }
}
