package VariationalSemanticTableaux;

import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;

import java.util.HashMap;

/**
 * Created by Nicolas FRANCOIS on 27/04/2023.
 */

public class AssociationsList {

    private HashMap<String, VariationSymbol> assocList;

    public AssociationsList() { assocList = new HashMap<>(); }

    public void addAssociation(String var, VariationSymbol vs) { assocList.put(var, vs); }

    public void removeAssociation(String var, VariationSymbol vs) { assocList.remove(var, vs); }

    public AssociationsList cloneList() {
        AssociationsList rep = new AssociationsList();
        assocList.forEach((key, value) -> { rep.addAssociation(key,value); });
        return rep; }
    public boolean contains(String var) { return assocList.containsKey(var); }

    public VariationSymbol getValue(String var) { return assocList.get(var); }

    public String toString() {
        StringBuilder res = new StringBuilder();
        assocList.forEach((key, value) -> {
            res.append(" (");
            res.append(key);
            res.append("^");
            res.append(value.toString());
            res.append(")");
        });
        return res.toString();
    }
}
