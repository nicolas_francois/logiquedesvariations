package Formulas.PropositionalFormulas.BinAryPropositionalFormulas;

import Formulas.PropositionalFormulas.NAryPropositionalFormulas.NAryConjunction;
import Formulas.PropositionalFormulas.NAryPropositionalFormulas.NAryDisjunction;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.PropositionalFormulas.UnAryPropositionalFormulas.Negation;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class ExclusiveOr extends BinAryFormula {

    public ExclusiveOr(PropositionalFormula p1, PropositionalFormula p2) {
        super(p1, p2);
    }

    @Override
    public PropositionalFormula toNFNF() {
        return new Disjunction(
                new Conjunction(prop1.toNFNF(), prop2.toNFNFneg()),
                new Conjunction(prop1.toNFNFneg(), prop2.toNFNF()));
    }

    @Override
    public PropositionalFormula toNFNFneg() {
        return new Disjunction(
                new Conjunction(prop1.toNFNF(), prop2.toNFNF()),
                new Conjunction(prop1.toNFNFneg(), prop2.toNFNFneg()));
    }

    public VariationalFormula toNFNFeqf() { return this.toNFNF().toNFNFeqf(); }

    public VariationalFormula toNFNFeqt() {
        return this.toNFNF().toNFNFeqt();
    }

    public VariationalFormula toNFNFpls() {
        return this.toNFNF().toNFNFpls();
    }

    public VariationalFormula toNFNFmns() {
        return this.toNFNF().toNFNFmns();
    }

    @Override
    public PropositionalDNFFormula toDNF() {
        NAryDisjunction nad = new NAryDisjunction();
        NAryConjunction nac1 = new NAryConjunction();
        nac1.addProposition(new Negation(prop1));
        nac1.addProposition(prop2);
        nad.addProposition(nac1);
        NAryConjunction nac2 = new NAryConjunction();
        nac2.addProposition(prop1);
        nac2.addProposition(new Negation(prop2));
        nad.addProposition(nac2);
        return nad.toDNF();
    }

    @Override
    public PropositionalDNFFormula toDNFneg() {
        NAryDisjunction nad = new NAryDisjunction();
        NAryConjunction nac1 = new NAryConjunction();
        nac1.addProposition(prop1);
        nac1.addProposition(prop2);
        nad.addProposition(nac1);
        NAryConjunction nac2 = new NAryConjunction();
        nac2.addProposition(new Negation(prop1));
        nac2.addProposition(new Negation(prop2));
        nad.addProposition(nac2);
        return nad.toDNF();
    }

    @Override
    public Boolean evaluateInitial(AssociationTable at) { return prop1.evaluateInitial(at) != prop2.evaluateInitial(at); }

    @Override
    public Boolean evaluateFinal(AssociationTable at) { return prop1.evaluateFinal(at) != prop2.evaluateFinal(at); }

    @Override
    public String toString() {
        return super.toString("XOR");
    }
}
