package Formulas.BaseVariationalFormulas;

import Formulas.VariationalFormulas.VariationalFormula;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 23/04/2023.
 */

public class BaseVarForm implements Iterable<VariationalFormula> {

    HashSet<VariationalFormula> hsvf;

    public BaseVarForm() { hsvf = new HashSet<>(); }

    public void addProposition(VariationalFormula p) { hsvf.add(p); }

    public Iterator<VariationalFormula> iterator() { return hsvf.iterator(); }

    public String toString() {
        StringBuilder res = new StringBuilder();
        for (VariationalFormula p: hsvf) {
            res.append(p.toString()).append("\n");
        }
        return res.toString();
    }
}
