package Formulas.VariationalFormulas.BinAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryConjunctionVar;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.UnAryVariationalFormulas.NegationVar;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class EquivalenceVar extends BinAryVarFormula {

    public EquivalenceVar(VariationalFormula p1, VariationalFormula p2) { super(p1, p2); }

    @Override
    public VariationalFormula toNFNF() {
        return new DisjunctionVar(
                new ConjunctionVar(prop1.toNFNF(), prop2.toNFNF()),
                new ConjunctionVar(prop1.toNFNFneg(), prop2.toNFNFneg())
        );
    }

    @Override
    public VariationalFormula toNFNFneg() {
        return new DisjunctionVar(
                new ConjunctionVar(prop1.toNFNF(), prop2.toNFNFneg()),
                new ConjunctionVar(prop1.toNFNFneg(), prop2.toNFNF())
        );
    }

    @Override
    public Boolean evaluate(AssociationTable at) { return prop1.evaluate(at) == prop2.evaluate(at); }

    public DNFFormula toDNF() {
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        NAryConjunctionVar nacv1 = new NAryConjunctionVar();
        nacv1.addProposition(prop1);
        nacv1.addProposition(prop2);
        nadv.addProposition(nacv1);
        NAryConjunctionVar nacv2 = new NAryConjunctionVar();
        nacv2.addProposition(new NegationVar(prop1));
        nacv2.addProposition(new NegationVar(prop2));
        nadv.addProposition(nacv2);
        return nadv.toDNF();
    }

    public DNFFormula toDNFneg() {
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        NAryConjunctionVar nacv1 = new NAryConjunctionVar();
        nacv1.addProposition(new NegationVar(prop1));
        nacv1.addProposition(prop2);
        nadv.addProposition(nacv1);
        NAryConjunctionVar nacv2 = new NAryConjunctionVar();
        nacv2.addProposition(prop1);
        nacv2.addProposition(new NegationVar(prop2));
        nadv.addProposition(nacv2);
        return nadv.toDNF();
    }

    @Override
    public String toString() {
        return super.toString("<=>");
    }
}
