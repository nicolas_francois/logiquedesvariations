package Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas;

import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalCube;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalLitteral;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.PropositionalFormulas.UnAryPropositionalFormulas.Negation;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.Atom;
import TruthTables.AssociationTable;

import java.util.HashSet;

public class Variable extends ZeroAryFormula {

    private final String name;

    public Variable(String n) {
        name = n;
    }

    @Override
    public HashSet<String> getVariables() {
        HashSet<String> varset = new HashSet<>();
        varset.add(name);
        return varset;
    }

    public String getName() { return name; }

    public PropositionalFormula toNFNFneg() { return new Negation(this); }

    public VariationalFormula toNFNFeqf() { return new Atom(name, new VariationSymbol(Symbol.EQUALSFALSE)); }
    public VariationalFormula toNFNFeqt() { return new Atom(name, new VariationSymbol(Symbol.EQUALSTRUE)); }
    public VariationalFormula toNFNFpls() { return new Atom(name, new VariationSymbol(Symbol.PLUS)); }
    public VariationalFormula toNFNFmns() { return new Atom(name, new VariationSymbol(Symbol.MINUS)); }

    @Override
    public PropositionalDNFFormula toDNF() {
        PropositionalLitteral l = new PropositionalLitteral(new Variable(name), true);
        PropositionalCube c = new PropositionalCube();
        c.addLitteral(l);
        PropositionalDNFFormula pf = new PropositionalDNFFormula();
        pf.addCube(c);
        return pf;
    }

    @Override
    public PropositionalDNFFormula toDNFneg() {
        PropositionalLitteral l = new PropositionalLitteral(new Variable(name), false);
        PropositionalCube c = new PropositionalCube();
        c.addLitteral(l);
        PropositionalDNFFormula pf = new PropositionalDNFFormula();
        pf.addCube(c);
        return pf;
    }

    public String toString() { return name; }

    @Override
    public Boolean evaluateInitial(AssociationTable at) {
        Symbol s = at.getSymbol(name).getSymbol();
        Boolean rep = false;
        switch (s) {
            case EQUALSFALSE:
                rep = false;
                break;
            case EQUALSTRUE:
                rep = true;
                return true;
            case PLUS:
                rep = false;
                break;
            default: // MINUS
                rep = true;
                break;
        }
        return rep;
    }

    @Override
    public Boolean evaluateFinal(AssociationTable at) {
        Symbol s = at.getSymbol(name).getSymbol();
        Boolean rep = false;
        switch (s) {
            case EQUALSFALSE:
                rep = false;
                break;
            case EQUALSTRUE:
                rep = true;
                return true;
            case PLUS:
                rep = true;
                break;
            default: // MINUS
                rep = false;
                break;
        }
        return rep;
    }
}
