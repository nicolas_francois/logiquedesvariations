package Formulas.VariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.Formula;
import TruthTables.AssociationTable;

import java.util.HashSet;

public abstract class VariationalFormula extends Formula {

    /**
     * The set of formulas composing the formula
     * @return a set of formulas
     */
    public abstract HashSet<VariationalFormula> getPropSet();

    /**
     * The set of variables composing the formula
     * @return a set of strings
     */
    public abstract HashSet<String> getVariables();

    /**
     * Constant symbol verification
     * @return true if formula is a constant, else false
     */
    public Boolean isConstant() { return false; }

    /**
     * Terminal symbol verification
     * @return true if formula is a variable, else false
     */
    public Boolean isVariable() { return false; }

    /**
     * isConjunction
     * @return true if formula is a conjunction (only usable for NNF)
     */
    public Boolean isConjunction() { return false; }

    /**
     * isDisjunction
     * @return true if formula is a disjunction (only usable for NNF)
     */
    public Boolean isDisjunction() { return false; }

    /**
     * Negative Normal Form
     * @return the negative normal form of the formula
     */
    public abstract VariationalFormula toNFNF();

    /**
     * Negative Normal Form of negation
     * @return the negative normal form of the negation of the formula
     */
    public abstract VariationalFormula toNFNFneg();

    public abstract Boolean evaluate(AssociationTable at);

    /**
     * Normal Disjunctive Variational Form
     * @return a disjunction of conjunctions of litteral variations equivalent to formula
     */
    public abstract DNFFormula toDNF();

    /**
     * Normal Disjunctive Variational Form of negation
     * @return a disjunction of conjunctions of litteral variations equivalent to the negation of formula
     */
    public DNFFormula toDNFneg() { return null; }
}
