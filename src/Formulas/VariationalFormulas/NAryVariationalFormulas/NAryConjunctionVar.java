package Formulas.VariationalFormulas.NAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFNAryConj;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFNAryDisj;
import Formulas.VariationalFormulas.UnAryVariationalFormulas.NegationVar;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class NAryConjunctionVar extends NAryVarFormula {

    public NAryConjunctionVar() {
        super();
    }

    @Override
    public Boolean isConjunction() { return true; }

    @Override
    public VariationalFormula toNFNF() {
        NAryConjunctionVar pnnf = new NAryConjunctionVar();
        for (VariationalFormula p: propSet) { pnnf.addProposition(p.toNFNF()); }
        return pnnf;
    }

    @Override
    public VariationalFormula toNFNFneg() {
        NAryDisjunctionVar pnnf = new NAryDisjunctionVar();
        for (VariationalFormula p: propSet) { pnnf.addProposition(p.toNFNFneg()); }
        return pnnf;
    }

    @Override
    public Boolean evaluate(AssociationTable at) {
        Boolean rep = true;
        VariationalFormula f;
        Iterator<VariationalFormula> it = propSet.iterator();
        while (it.hasNext() && rep) {
            f = it.next();
            rep = rep && f.evaluate(at);
        }
        return rep;
    }

    public DNFFormula toDNF() {
        DNFNAryConj dnfc = new DNFNAryConj();
        for (VariationalFormula f: propSet) { dnfc.addProposition(f.toDNF()); }
        return dnfc.toDNF();
    }

    public DNFFormula toDNFneg() {
        DNFNAryDisj dnfd = new DNFNAryDisj();
        for (VariationalFormula f: propSet) { dnfd.addProposition((new NegationVar(f)).toDNF()); }
        return dnfd.toDNF();
    }


    @Override
    public String toString() { return super.toString("AND"); }
}
