package Formulas.VariationalFormulas.VariationSymbols;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public enum Symbol {
    EQUALSTRUE,  // Always true
    EQUALSFALSE, // Always false
    PLUS,        // False, then true
    MINUS,       // True, then false
    DIFF,        // Not the same value (true then false, or false then true)
    EQUALS,      // Same value (true then true, or false then false)
    LEFTTRUE,    // True, then any
    LEFTFALSE,   // False, then any
    RIGHTTRUE,   // Any, then true
    RIGHTFALSE;  // Any, then false
}
