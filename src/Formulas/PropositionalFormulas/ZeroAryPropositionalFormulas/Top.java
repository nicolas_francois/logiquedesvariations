package Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas;

import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNF;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.BottomVar;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.TopVar;
import TruthTables.AssociationTable;

public class Top extends Constant {

    public Top() { super(true); }

    public PropositionalFormula toNFNFneg() { return new Bottom(); }

    public VariationalFormula toNFNFeqf() { return new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); }
    public VariationalFormula toNFNFeqt() { return new TopVar(new VariationSymbol(Symbol.EQUALSTRUE)); }
    public VariationalFormula toNFNFpls() { return new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); }
    public VariationalFormula toNFNFmns() { return new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); }

    public PropositionalDNFFormula toDNF() { return new PropositionalDNFFormula(); }
    public PropositionalDNFFormula toDNFneg() { return null; }

    @Override
    public Boolean evaluateInitial(AssociationTable at) { return true; }

    @Override
    public Boolean evaluateFinal(AssociationTable at) { return true; }
}
