package TruthTables;

import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 01/05/2023.
 */

public class AssociationTable implements Iterable<Association> {

    private ArrayList<Association> associations;

    public AssociationTable(HashSet<String> vl) {
        associations = new ArrayList<>();
        for (String v : vl) {
            associations.add(new Association(v));
        }
    }

    public VariationSymbol getSymbol(String v) {
        VariationSymbol vs = new VariationSymbol(Symbol.EQUALSFALSE);
        for (Association a : associations) {
            if (a.getName().equals(v)) {
                vs = a.getVs();
            }
        }
        return vs;
    }

    public void iterAssoc() {
        Boolean carryover = associations.get(0).carry();
        associations.get(0).iterSymbol();
        int i = 1;
        while (i < associations.size() && carryover) {
            carryover = associations.get(i).carry();
            associations.get(i).iterSymbol();
            i += 1;
        }
    }

    public Boolean endIter() {
        Boolean rep = true;
        int i = 0;
        while (i < associations.size() && rep) {
            if (!associations.get(i).getVs().match(Symbol.MINUS)) { rep = false; }
            i += 1;
        }
        return rep;
    }

    @Override
    public Iterator<Association> iterator() {
        return associations.iterator();
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        for (Association as: associations) {
            res.append(as.getName());
            res.append(":");
            res.append(as.getVs().toString());
            res.append(" ");
        }
        return res.toString();
    }
}
