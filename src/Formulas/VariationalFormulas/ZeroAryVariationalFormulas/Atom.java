package Formulas.VariationalFormulas.ZeroAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.Cube;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.CubesList;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.VariableVariation;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

import java.util.HashSet;

import static Formulas.VariationalFormulas.VariationSymbols.Symbol.*;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class Atom extends ZeroAryVarFormula {

    private String name;

    public Atom(String n, VariationSymbol v) {
        super(v);
        name = n; }

    @Override
    public Boolean isVariable() { return true; }

    public String getName() { return name; }

    @Override
    public HashSet<String> getVariables() {
        HashSet<String> varset = new HashSet<>();
        varset.add(name);
        return varset;
    }

    @Override
    public VariationalFormula toNFNF() {
        return this;
    }

    @Override
    public VariationalFormula toNFNFneg() {
        NAryDisjunctionVar res = new NAryDisjunctionVar();
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSFALSE)));
                res.addProposition(new Atom(name, new VariationSymbol(PLUS)));
                res.addProposition(new Atom(name, new VariationSymbol(MINUS)));
                break;
            case EQUALSFALSE:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSTRUE)));
                res.addProposition(new Atom(name, new VariationSymbol(PLUS)));
                res.addProposition(new Atom(name, new VariationSymbol(MINUS)));
                break;
            case PLUS:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSTRUE)));
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSFALSE)));
                res.addProposition(new Atom(name, new VariationSymbol(MINUS)));
                break;
            case MINUS:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSTRUE)));
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSFALSE)));
                res.addProposition(new Atom(name, new VariationSymbol(PLUS)));
                break;
            case DIFF:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSTRUE)));
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSFALSE)));
                break;
            case EQUALS:
                res.addProposition(new Atom(name, new VariationSymbol(PLUS)));
                res.addProposition(new Atom(name, new VariationSymbol(MINUS)));
                break;
            case LEFTTRUE:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSFALSE)));
                res.addProposition(new Atom(name, new VariationSymbol(PLUS)));
                break;
            case LEFTFALSE:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSTRUE)));
                res.addProposition(new Atom(name, new VariationSymbol(MINUS)));
                break;
            case RIGHTTRUE:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSFALSE)));
                res.addProposition(new Atom(name, new VariationSymbol(MINUS)));
                break;
            case RIGHTFALSE:
                res.addProposition(new Atom(name, new VariationSymbol(EQUALSTRUE)));
                res.addProposition(new Atom(name, new VariationSymbol(PLUS)));
                break;
        }
        return res;
    }

    @Override
    public Boolean evaluate(AssociationTable at) {
        // DEBUG
        // System.out.println("Évaluation de "+this.toString()+" :");
        // System.out.println("Table d'associations : "+at.toString());
        // System.out.println("Symbole dans la table : "+at.getSymbol(name));
        // System.out.println("Symbole de la variable : "+vs.getSymbol());
        // System.out.println("Comparaison : "+at.getSymbol(name).equals(vs.getSymbol()));
        // FIN DEBUG
        return vs.match(at.getSymbol(name).getSymbol());
    }

    public DNFFormula toDNF() {
        CubesList cl = new CubesList();
        Cube eqt = new Cube();
        eqt.addVarVar(new VariableVariation(name,true,true));
        Cube eqf = new Cube();
        eqf.addVarVar(new VariableVariation(name,false,false));
        Cube plus = new Cube();
        plus.addVarVar(new VariableVariation(name,false,true));
        Cube minus = new Cube();
        minus.addVarVar(new VariableVariation(name,true,false));
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
                cl.addCube(eqt);
                break;
            case EQUALSFALSE:
                cl.addCube(eqf);
                break;
            case PLUS:
                cl.addCube(plus);
                break;
            case MINUS:
                cl.addCube(minus);
                break;
            case DIFF:
                cl.addCube(plus);
                cl.addCube(minus);
                break;
            case EQUALS:
                cl.addCube(eqt);
                cl.addCube(eqf);
                break;
            case LEFTTRUE:
                cl.addCube(eqt);
                cl.addCube(minus);
                break;
            case LEFTFALSE:
                cl.addCube(eqf);
                cl.addCube(plus);
                break;
            case RIGHTTRUE:
                cl.addCube(eqt);
                cl.addCube(plus);
                break;
            case RIGHTFALSE:
                cl.addCube(eqf);
                cl.addCube(minus);
                break;
        }
        return new DNFFormula(cl);
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(name);
        res.append("^");
        res.append(vs.toString());
        return res.toString();
    }
}
