package Formulas.BasePropositionalFormulas;

import Formulas.PropositionalFormulas.PropositionalFormula;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 23/04/2023.
 */

public class BasePropForm implements Iterable<PropositionalFormula> {

    HashSet<PropositionalFormula> hspf;

    public void addProposition(PropositionalFormula p) { hspf.add(p); }

    public Iterator<PropositionalFormula> iterator() { return hspf.iterator(); }

    public String toString() {
        StringBuilder res = new StringBuilder();
        for (PropositionalFormula p: hspf) {
            res.append(p.toString()).append("\n");
        }
        return res.toString();
    }
}
