package Formulas.VariationalFormulas.NAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFNAryConj;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFNAryDisj;
import Formulas.VariationalFormulas.UnAryVariationalFormulas.NegationVar;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class NAryDisjunctionVar extends NAryVarFormula {

    public NAryDisjunctionVar() {
        super();
    }

    @Override
    public Boolean isDisjunction() { return true; }

    @Override
    public VariationalFormula toNFNF() {
        NAryDisjunctionVar pnnf = new NAryDisjunctionVar();
        for (VariationalFormula p: propSet) { pnnf.addProposition(p.toNFNF()); }
        return pnnf;
    }

    @Override
    public VariationalFormula toNFNFneg() {
        NAryConjunctionVar pnnf = new NAryConjunctionVar();
        for (VariationalFormula p: propSet) { pnnf.addProposition(p.toNFNFneg()); }
        return pnnf;
    }

    public DNFFormula toDNF() {
        DNFNAryDisj dnfd = new DNFNAryDisj();
        for (VariationalFormula f: propSet) { dnfd.addProposition(f.toDNF()); }
        return dnfd.toDNF();
    }

    public DNFFormula toDNFneg() {
        DNFNAryConj dnfc = new DNFNAryConj();
        for (VariationalFormula f: propSet) { dnfc.addProposition((new NegationVar(f)).toDNF()); }
        return dnfc.toDNF();
    }

    @Override
    public Boolean evaluate(AssociationTable at) {
        Boolean rep = false;
        VariationalFormula f;
        Iterator<VariationalFormula> it = propSet.iterator();
        while (it.hasNext() && !rep) {
            f = it.next();
            rep = rep || f.evaluate(at);
        }
        return rep;
    }

    @Override
    public String toString() { return super.toString("OR"); }
}
