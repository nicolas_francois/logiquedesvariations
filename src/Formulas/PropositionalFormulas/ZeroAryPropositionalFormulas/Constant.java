package Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas;

import java.util.HashSet;

public abstract class Constant extends ZeroAryFormula {

    Boolean value;

    public Constant(Boolean b) { value = b; }

    @Override
    public HashSet<String> getVariables() { return new HashSet<>(); }

    public String toString() {
        if (value) { return "T"; } else { return "F"; }
    }
}
