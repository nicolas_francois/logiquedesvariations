package Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms;

import Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas.Variable;
import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.VariableVariation;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class PropositionalCube implements Iterable<PropositionalLitteral> {

    private ArrayList<PropositionalLitteral> litteralSet;

    public PropositionalCube() { litteralSet = new ArrayList<>(); }

    public void addLitteral(PropositionalLitteral l) { litteralSet.add(l); }

    public ArrayList<PropositionalLitteral> getVarSet() { return litteralSet; }

    public Iterator<PropositionalLitteral> iterator() { return litteralSet.iterator(); }

    public PropositionalCube timesLitteral(PropositionalLitteral l) {
        PropositionalCube rep = new PropositionalCube();
        // If the list is empty, then it represents true
        Iterator<PropositionalLitteral> itlit = litteralSet.iterator();
        Boolean sameDetected = false;
        Boolean incompatibleDetected = false;
        // We look for each litteral in the present cube
        while (itlit.hasNext() && !incompatibleDetected) {
            PropositionalLitteral ll = itlit.next();
            // If a litteral identical to v was already detected, we just copy the end of the cube
            if (sameDetected) { rep.addLitteral(ll); }
            // else if the present litteral has the same variable...
            else if (ll.getVariable().equals(l.getVariable())) {
                // and the same "signs", we just copy the end of the cube
                if (ll.getSign().equals(l.getSign())) {
                    sameDetected = true;
                    rep.addLitteral(ll);
                }
                // else there is an incompatibility, we just return null (considered as equivalent to false)
                else {
                    incompatibleDetected = true;
                }
            }
        }
        if (incompatibleDetected) { return null; }
        else {
            // If no litteral was equal to v, we add it at the end of the list
            if (!sameDetected) { rep.addLitteral(l); }
            return rep;
        }
    }

    public PropositionalCube timesCube(PropositionalCube c) {
        if (c == null) { return null; }
        PropositionalCube rep = new PropositionalCube();
        for (PropositionalLitteral ll: litteralSet) { rep.addLitteral(ll); }
        for (PropositionalLitteral ll: c) {
            rep = rep.timesLitteral(ll);
            if (rep == null) { return null; }
        }
        return rep;
    }
}
