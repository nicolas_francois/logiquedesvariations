package Formulas.PropositionalFormulas;

import Formulas.Formula;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

import java.util.HashSet;

public abstract class PropositionalFormula extends Formula {

    /**
     * The set of formulas composing the formula
     * @return a set of formulas
     */
    public abstract HashSet<PropositionalFormula> getPropSet();

    /**
     * The set of variables composing the formula
     * @return a set of strings
     */
    public abstract HashSet<String> getVariables();

    /**
     * Negative Normal Form
     * @return the negative normal form of the formula
     */
    public abstract PropositionalFormula toNFNF();

    /**
     * Negative Normal Form of negation
     * @return the negative normal form of the negation of the formula
     */
    public abstract PropositionalFormula toNFNFneg();

    /**
     * Negative Normal Forms of variational formulas
     * - toNNFeqf : returns the NNF of prop^eqf
     * - toNNFeqt : returns the NNF of prop^eqt
     * - toNNFpls : returns the NNF of prop^plus
     * - toNNFmns : returns the NNF of prop^minus
     * @return a variational formula in NNF
     */
    public abstract VariationalFormula toNFNFeqf();
    public abstract VariationalFormula toNFNFeqt();
    public abstract VariationalFormula toNFNFpls();
    public abstract VariationalFormula toNFNFmns();

    /**
     * Disjunctive Normal Forms of propositional formulas
     * - toDNF: returns the DNF of prop
     * - toDNFneg: returns the DNF of negation of prop
     * @return a propositional formula in DNF
     */
    public abstract PropositionalDNFFormula toDNF();
    public abstract PropositionalDNFFormula toDNFneg();

    /**
     * Truth value of propositional formula according to an association table
     * - evaluateInitial: returns the truth value according to first component
     * - evaluateFinal: returns the truth value according to second component
     * @return a boolean
     */
    public abstract Boolean evaluateInitial(AssociationTable at);
    public abstract Boolean evaluateFinal(AssociationTable at);
}
