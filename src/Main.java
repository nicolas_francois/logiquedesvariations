import Analyse.AnalyseurLexical;
import Analyse.AnalyseurSyntaxique;
import Formulas.BaseVariationalFormulas.BaseVarForm;
import Exceptions.AnalyseException;
import Formulas.VariationalFormulas.BinAryVariationalFormulas.ImplicationVar;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.VariationalTruthTable;
// import VariationalSemanticTableaux.VariationalTableau;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public Main(String nomFichier) {
        try {
            AnalyseurSyntaxique analyseur = new AnalyseurSyntaxique(new AnalyseurLexical(new FileReader(nomFichier)));
            BaseVarForm base = (BaseVarForm) analyseur.parse().value;
            System.out.println("Lecture OK");
            // System.out.println(base.toString());
            /* for (VariationalFormula p: base) {
                System.out.println("Formule :");
                System.out.println("---------");
                System.out.println(p.toString());
                System.out.println("Forme normale négative :");
                VariationalFormula fnnp = p.toNFNF();
                System.out.println(fnnp.toString());
                System.out.println("Tautologie :");
                VariationalTableau vt = new VariationalTableau(p);
                if (vt.isTautology()) {
                    System.out.println("Oui");
                } else {
                    System.out.println("Non");
                    System.out.println("Réfutations :");
                    System.out.println(vt.refutationsToString());
                }
                System.out.println("Table de vérité :");
                VariationalTruthTable vtt = new VariationalTruthTable(p);
                System.out.println(vtt.getTruthTable());
            } */
            Iterator<VariationalFormula> baseiter = base.iterator();
            VariationalFormula phi = baseiter.next();
            VariationalFormula psi = baseiter.next();
            VariationalFormula phientpsi = new ImplicationVar(phi,psi);
            VariationalFormula psientphi = new ImplicationVar(psi,phi);
        }
        catch (FileNotFoundException ex) {
            System.err.println("Fichier " + nomFichier + " inexistant") ;
        }
        catch (AnalyseException ex) {
            System.err.println(ex.getMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Nombre incorrect d'arguments") ;
            System.err.println("\tjava -jar TableauSemantique.jar <fichierBase.bts>") ;
            System.exit(1) ;
        }
        new Main(args[0]);
    }
}
