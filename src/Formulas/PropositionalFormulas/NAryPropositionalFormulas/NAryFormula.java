package Formulas.PropositionalFormulas.NAryPropositionalFormulas;

import Formulas.Formula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.VariationalFormulas.VariationalFormula;

import java.util.HashSet;
import java.util.Iterator;

public abstract class NAryFormula extends PropositionalFormula {

    protected HashSet<PropositionalFormula> propSet;

    public NAryFormula() {
        propSet = new HashSet<>();
    }

    public void addProposition(PropositionalFormula p) {
        propSet.add(p);
    }

    public HashSet<PropositionalFormula> getPropSet() {
        HashSet<PropositionalFormula> hspf = new HashSet<>();
        for (PropositionalFormula f: propSet) { hspf.add(f); }
        return hspf;
    }

    @Override
    public HashSet<String> getVariables() {
        HashSet<String> navfvars = new HashSet<>();
        for (Formula f: propSet) {
            for (String v: f.getVariables()) { navfvars.add(v); }
        }
        return navfvars;
    }

    public abstract PropositionalFormula toBinary();

    public VariationalFormula toNFNFeqf() {
        // System.out.println("Appel de toNNFeqf");
        return this.toBinary().toNFNFeqf();
    }

    public VariationalFormula toNFNFeqt() {
        // System.out.println("Appel de toNNFeqt");
        return this.toBinary().toNFNFeqt();
    }

    public VariationalFormula toNFNFpls() {
        // System.out.println("Appel de toNNFpls");
        return (this.toBinary()).toNFNFpls();
    }

    public VariationalFormula toNFNFmns() {
        // System.out.println("Appel de toNNFmns");
        return (this.toBinary()).toNFNFmns();
    }

    public String toString(String op) {
        StringBuilder res = new StringBuilder();
        if (propSet.size() == 0) { return res.toString(); } // Should never occur !!!
        else { // Even case "propSet.size() == 1" should not occur
            res.append("(");
            Iterator<PropositionalFormula> propIt = propSet.iterator();
            res.append(propIt.next().toString());
            while (propIt.hasNext()) {
                res.append(" ").append(op).append(" ").append(propIt.next());
            }
            res.append(")");
            return res.toString();
        }
    }
}
