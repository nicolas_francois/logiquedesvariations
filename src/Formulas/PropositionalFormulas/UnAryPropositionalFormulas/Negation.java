package Formulas.PropositionalFormulas.UnAryPropositionalFormulas;

import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.VariationalFormulas.VarPropositionalFormulas.VarPropFormula;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

public class Negation extends UnAryFormula {

    public Negation(PropositionalFormula p) {
        prop = p;
    }

    public PropositionalFormula toNFNF() {
        // System.out.println("PropNegationNNF : "+prop.toString());
        return (PropositionalFormula) prop.toNFNFneg();
    }

    public PropositionalFormula toNFNFneg() {
        // System.out.println("PropNegationNNFneg : "+prop.toString());
        return (PropositionalFormula) prop.toNFNF();
    }

    public VariationalFormula toNFNFeqf() {
        // System.out.println("PropNegationNNFeqf : "+prop.toString());
        return (new VarPropFormula(prop, new VariationSymbol(Symbol.EQUALSTRUE))).toNFNF();
    }

    public VariationalFormula toNFNFeqt() {
        // System.out.println("PropNegationNNFeqt : "+prop.toString());
        return (new VarPropFormula(prop, new VariationSymbol(Symbol.EQUALSFALSE))).toNFNF();
    }

    public VariationalFormula toNFNFpls() {
        // System.out.println("PropNegationNNFpls : "+prop.toString());
        return (new VarPropFormula(prop, new VariationSymbol(Symbol.MINUS))).toNFNF();
    }

    public VariationalFormula toNFNFmns() {
        // System.out.println("PropNegationNNFmns : "+prop.toString());
        return (new VarPropFormula(prop, new VariationSymbol(Symbol.PLUS))).toNFNF();
    }

    @Override
    public PropositionalDNFFormula toDNF() { return prop.toDNFneg(); }

    @Override
    public PropositionalDNFFormula toDNFneg() { return prop.toDNF(); }

    @Override
    public Boolean evaluateInitial(AssociationTable at) { return !prop.evaluateInitial(at); }

    @Override
    public Boolean evaluateFinal(AssociationTable at) { return !prop.evaluateFinal(at); }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("(NOT ");
        res.append(prop.toString());
        res.append(")");
        return res.toString();
    }
}
