package Formulas.VariationalFormulas.VarPropositionalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryDisjunctionVar;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.BottomVar;
import TruthTables.AssociationTable;

import java.util.HashSet;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class VarPropFormula extends VariationalFormula {

    private PropositionalFormula prop;
    private VariationSymbol vs;

    public VarPropFormula(PropositionalFormula f, VariationSymbol v) {
        prop = f;
        vs = v;
    }

    @Override
    public HashSet<VariationalFormula> getPropSet() {
        // Irrelevant for this case, tableaux works with negative normal form
        return null;
    }

    @Override
    public HashSet<String> getVariables() { return prop.getVariables(); }

    @Override
    public VariationalFormula toNFNF() {
        VariationalFormula res = new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); // En attendant
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
                res = prop.toNFNFeqt();
                break;
            case EQUALSFALSE:
                res = prop.toNFNFeqf();
                break;
            case PLUS:
                res = prop.toNFNFpls();
                break;
            case MINUS:
                res = prop.toNFNFmns();
                break;
            case DIFF:
                nadv.addProposition(prop.toNFNFpls());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
            case EQUALS:
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFeqf());
                res = nadv;
                break;
            case LEFTTRUE:
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
            case LEFTFALSE:
                nadv.addProposition(prop.toNFNFeqf());
                nadv.addProposition(prop.toNFNFpls());
                res = nadv;
                break;
            case RIGHTTRUE:
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFpls());
                res = nadv;
                break;
            case RIGHTFALSE:
                nadv.addProposition(prop.toNFNFeqf());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
        }
        return res;
    }

    @Override
    public VariationalFormula toNFNFneg() {
        VariationalFormula res = new BottomVar(new VariationSymbol(Symbol.EQUALSTRUE)); // En attendant
        NAryDisjunctionVar nadv = new NAryDisjunctionVar();
        switch (vs.getSymbol()) {
            case EQUALSTRUE:
                nadv.addProposition(prop.toNFNFeqf());
                nadv.addProposition(prop.toNFNFpls());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
            case EQUALSFALSE:
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFpls());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
            case PLUS:
                nadv.addProposition(prop.toNFNFeqf());
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
            case MINUS:
                nadv.addProposition(prop.toNFNFeqf());
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFpls());
                res = nadv;
                break;
            case EQUALS:
                nadv.addProposition(prop.toNFNFpls());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
            case DIFF:
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFeqf());
                res = nadv;
                break;
            case LEFTFALSE:
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
            case LEFTTRUE:
                nadv.addProposition(prop.toNFNFeqf());
                nadv.addProposition(prop.toNFNFpls());
                res = nadv;
                break;
            case RIGHTFALSE:
                nadv.addProposition(prop.toNFNFeqt());
                nadv.addProposition(prop.toNFNFpls());
                res = nadv;
                break;
            case RIGHTTRUE:
                nadv.addProposition(prop.toNFNFeqf());
                nadv.addProposition(prop.toNFNFmns());
                res = nadv;
                break;
        }
        return res;
    }

    @Override
    public Boolean evaluate(AssociationTable at) {
        Boolean rep = false;
        Boolean propEvalBefore = prop.evaluateInitial(at);
        Boolean propEvalAfter = prop.evaluateFinal(at);
        switch (vs.getSymbol()) {
            case EQUALSFALSE:
                rep = !propEvalBefore && ! propEvalAfter;
                break;
            case EQUALSTRUE:
                rep = propEvalBefore && propEvalAfter;
                break;
            case PLUS:
                rep = !propEvalBefore && propEvalAfter;
                break;
            default: // MINUS
                rep = propEvalBefore && !propEvalAfter;
                break;
        }
        return rep;
    }

    public DNFFormula toDNF() { return null; }

    public DNFFormula toDNFneg() { return null; }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("(");
        res.append(prop.toString());
        res.append(")^");
        res.append(vs.toString());
        return res.toString();
    }
}
