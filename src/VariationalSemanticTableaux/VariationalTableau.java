package VariationalSemanticTableaux;

import Formulas.Formula;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.ConstantVar;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.Atom;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 27/04/2023.
 */

public class VariationalTableau {

    private VariationalFormula prop;
    private ArrayList<AssociationsList> refutations;

    public VariationalTableau(VariationalFormula p) {
        // We prove p is a tautology by proving its negation is not satisfiable
        prop = p.toNFNFneg();
        refutations = new ArrayList<>();
        proof();
    }

    public Boolean isTautology() {
        return refutations.size() == 0;
    }

    public String refutationsToString() {
        StringBuilder res = new StringBuilder();
        for (AssociationsList ref : refutations) {
            System.out.println(ref.toString());
        }
        return res.toString();
    }

    public ArrayList<AssociationsList> getRefutations() {
        return refutations;
    }

    private void proofaux(ArrayList<VariationalFormula> vfal, AssociationsList atoms, int firstNotUsed, int lastToProve) {
        // DEBUG
        /* System.out.println("* proofaux called with arguments : firstNotUsed = " + firstNotUsed + ", lastToProve = " + lastToProve);
        System.out.println("  List of propositions :");
        for (VariationalFormula f : vfal) {
            System.out.println("  " + f.toString());
        } */
        // FIN DEBUG
        if (firstNotUsed > lastToProve) {
            refutations.add(atoms);
        } else {
            // Declaration of the set of sub-formulas
            HashSet<VariationalFormula> cprops;
            Boolean foundConjunction = false;
            int firstConjunction = -1;
            for (int i = firstNotUsed; i <= lastToProve; i++) {
                if (vfal.get(i).isConjunction()) {
                    if (!foundConjunction) {
                        foundConjunction = true;
                        firstConjunction = i;
                    }
                }
            }
            // Now,
            // -> foundConjunction is true if vfal contains a conjunction,
            //    firstConjunction is index of first found conjunction
            if (foundConjunction) {
                // We have a conjunction, we treat it first,
                // by swapping it with the first unused formula
                // DEBUG
                // System.out.println("We found a conjunction");
                // System.out.println("First conjunction index : "+ firstConjunction);
                // FIN DEBUG
                VariationalFormula temp = vfal.get(firstConjunction);
                vfal.set(firstConjunction, vfal.get(firstNotUsed));
                vfal.set(firstNotUsed, temp);
                // DEBUG
                // System.out.println("List of propositions after exchange :");
                // for (VariationalFormula f: vfal) { System.out.println("  "+f.toString()); }
                // FIN DEBUG
                // We get the propositions from the first formula to develop
                cprops = vfal.get(firstNotUsed).getPropSet();
                // We create a copy of list atoms
                AssociationsList newatoms = atoms.cloneList();
                Boolean conflict = false;
                Iterator<VariationalFormula> itf = cprops.iterator();
                // For every sub-formula of firstConjunction, if there is no conflict found
                while (itf.hasNext() && !conflict) {
                    VariationalFormula f = (VariationalFormula) itf.next();
                    if (f.isConstant()) {
                        if (!((ConstantVar) f).getValue()) {
                            // If we encounter Bottom, there is an immediate conflict
                            conflict = true;
                        }
                        // else it is Top, and we simply continue (no need to insert it in vfal)
                    }
                    // If it is a variable
                    else if (f.isVariable()) {
                        // and if the variable is already present in atoms
                        if (newatoms.contains(((Atom) f).getName())) {
                            // and if it is with a different variation symbol
                            if (!newatoms.getValue(((Atom) f).getName()).equals(((Atom) f).getVS())) {
                                // then there is a conflict on this branch
                                conflict = true;
                            }
                            // else there is nothing to do
                        }
                        // else the variable and the symbols are added to the already encountered symbols
                        else {
                            newatoms.addAssociation(((Atom) f).getName(), ((Atom) f).getVS());
                        }
                    }
                    // else f is a conjunction or a disjunction, we add it at the end of the list vfal
                    else {
                        vfal.ensureCapacity(lastToProve + 5);
                        lastToProve += 1;
                        if (vfal.size() == lastToProve) {
                            vfal.add(f);
                        } else {
                            vfal.set(lastToProve, f);
                        }
                    }
                }
                // if there is no conflict found, we just call proofaux with the new lists and indexes
                if (!conflict) {
                    proofaux(vfal, newatoms, firstNotUsed + 1, lastToProve);
                }
            }
            // There is no conjunction in the list, develop first conjunction
            else {
                // DEBUG
                // System.out.println("Only disjunctions");
                // We get the propositions from the first formula to develop
                cprops = vfal.get(firstNotUsed).getPropSet();
                // We develop a branch for every sub-formula of f
                for (Formula f : cprops) {
                    if (((VariationalFormula) f).isConstant()) {
                        if (((ConstantVar) f).getValue()) {
                            // If we encounter Top, we simply call proofaux recursively
                            proofaux(vfal, atoms, firstNotUsed + 1, lastToProve);
                        }
                        // else it is Bottom, and we simply continue
                    }
                    // If it is a variable
                    else if (((VariationalFormula) f).isVariable()) {
                        // and if the variable is already present in atoms
                        if (atoms.contains(((Atom) f).getName())) {
                            // and if it is the same variation symbol
                            if (atoms.getValue(((Atom) f).getName()).equals(((Atom) f).getVS())) {
                                // we simply call proofaux recursively
                                proofaux(vfal, atoms, firstNotUsed + 1, lastToProve);
                            }
                            // else there is a conflict, nothing to do
                        }
                        // else the variable and the symbols are added to the already encountered symbols
                        else {
                            AssociationsList newatoms = atoms.cloneList();
                            newatoms.addAssociation(((Atom) f).getName(), ((Atom) f).getVS());
                            proofaux(vfal, newatoms, firstNotUsed + 1, lastToProve);
                        }
                    }
                    // else f is a conjunction or a disjunction, we add it at the end of the list vfal
                    else {
                        vfal.ensureCapacity(lastToProve + 1);
                        lastToProve += 1;
                        if (vfal.size() == lastToProve) {
                            vfal.add((VariationalFormula) f);
                        } else {
                            vfal.set(lastToProve, (VariationalFormula) f);
                        }
                        // vfal.set(lastToProve, (VariationalFormula) f);
                        proofaux(vfal, atoms, firstNotUsed + 1, lastToProve);
                    }
                }
            }
        }
    }

    public void proof() {
        ArrayList<VariationalFormula> toProve = new ArrayList<>();
        toProve.add(prop);
        int firstNotUsed = 0;
        int lastToProve = 0;
        proofaux(toProve, new AssociationsList(), firstNotUsed, lastToProve);
    }
}
