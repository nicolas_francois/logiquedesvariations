package Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas;

import Formulas.Formula;
import Formulas.PropositionalFormulas.PropositionalFormula;

import java.util.HashSet;

public abstract class ZeroAryFormula extends PropositionalFormula {

    @Override
    public HashSet<PropositionalFormula> getPropSet() {
        HashSet<PropositionalFormula> hspf = new HashSet<>();
        hspf.add(this);
        return hspf;
    }

    // The NNF of a 0-ary formula is the formula itself
    public PropositionalFormula toNFNF() { return this; }
}
