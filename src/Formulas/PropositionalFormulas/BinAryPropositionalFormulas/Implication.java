package Formulas.PropositionalFormulas.BinAryPropositionalFormulas;

import Formulas.PropositionalFormulas.NAryPropositionalFormulas.NAryConjunction;
import Formulas.PropositionalFormulas.NAryPropositionalFormulas.NAryDisjunction;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.PropositionalFormulas.UnAryPropositionalFormulas.Negation;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

public class Implication extends BinAryFormula {

    public Implication(PropositionalFormula p1, PropositionalFormula p2) {
        super(p1, p2);
    }

    @Override
    public PropositionalFormula toNFNF() {
        return new Disjunction(prop1.toNFNFneg(), prop2.toNFNF());
    }

    @Override
    public PropositionalFormula toNFNFneg() {
        return new Conjunction(prop1.toNFNF(), prop2.toNFNFneg());
    }

    public VariationalFormula toNFNFeqf() { return this.toNFNF().toNFNFeqf(); }

    public VariationalFormula toNFNFeqt() {
        return this.toNFNF().toNFNFeqt();
    }

    public VariationalFormula toNFNFpls() {
        return this.toNFNF().toNFNFpls();
    }

    public VariationalFormula toNFNFmns() {
        return this.toNFNF().toNFNFmns();
    }

    @Override
    public PropositionalDNFFormula toDNF() {
        NAryDisjunction nad = new NAryDisjunction();
        nad.addProposition(new Negation(prop1));
        nad.addProposition(prop2);
        return nad.toDNF();
    }

    @Override
    public PropositionalDNFFormula toDNFneg() {
        NAryConjunction nac = new NAryConjunction();
        nac.addProposition(prop1);
        nac.addProposition(new Negation(prop2));
        return nac.toDNF();
    }

    @Override
    public Boolean evaluateInitial(AssociationTable at) { return !prop1.evaluateInitial(at) || prop2.evaluateInitial(at); }

    @Override
    public Boolean evaluateFinal(AssociationTable at) { return !prop1.evaluateFinal(at) || prop2.evaluateFinal(at); }

    @Override
    public String toString() {
        return super.toString("=>");
    }
}
