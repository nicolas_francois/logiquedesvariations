package Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms;

/**
 * Created by Nicolas FRANCOIS on 16/05/2023.
 */

public class PropositionalDNFFormula {

    // The list of cubes of the disjunction.
    // An empty list represents bottom
    // A null DNFFormula represents top
    protected PropositionalCubesList cubes;

    public PropositionalDNFFormula() { cubes = new PropositionalCubesList(); }

    public PropositionalDNFFormula(PropositionalCubesList cl) { cubes = cl; }

    public void addCube(PropositionalCube c) { cubes.addCube(c); }

    public void addCubesList(PropositionalCubesList cl) {
        for (PropositionalCube c : cl) { cubes.addCube(c); }
    }

    public PropositionalCubesList getCubes() { return cubes; }
}
