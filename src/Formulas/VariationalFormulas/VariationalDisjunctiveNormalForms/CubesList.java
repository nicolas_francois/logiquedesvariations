package Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms;

import TruthTables.AssociationTable;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 12/05/2023.
 */

public class CubesList implements Iterable<Cube> {

    private ArrayList<Cube> alCubes;

    public CubesList() { alCubes = new ArrayList<>(); }
    public CubesList(ArrayList<Cube> alc) { alCubes = alc; }

    public void addCube(Cube c) { alCubes.add(c); }

    public void addCubesList(CubesList cl) { for (Cube c: cl) { addCube(c); }}

    public Iterator<Cube> iterator() { return alCubes.iterator(); }

    public CubesList timesCube(Cube c) {
        CubesList rep = new CubesList();
        if (c == null) { return rep; }
        Cube temp;
        for (Cube cc: alCubes) {
            temp = cc.timesCube(c);
            if (temp != null) { rep.addCube(temp); }
        }
        return rep;
    }

    public CubesList timesCubesList(CubesList cl) {
        if (cl == null) { return null; }
        CubesList rep = new CubesList();
        CubesList temp;
        for (Cube c: alCubes) { rep.addCube(c); }
        if (cl == null) { return rep; }
        for (Cube c1: alCubes) {
            temp = this.timesCube(c1);
            if (temp != null) { rep.addCubesList(temp); }
        }
        return rep;
    }

    public int getSize() { return alCubes.size(); }

    public Boolean evaluate(AssociationTable at) {
        Boolean rep = false;
        Iterator<Cube> cubeIterator = alCubes.iterator();
        while (cubeIterator.hasNext() && !rep) {
            rep = rep || cubeIterator.next().evaluate(at);
        }
        return rep;
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        if (alCubes.size() == 0) { res.append("True"); }
        else {
            Iterator<Cube> cubeIterator = alCubes.iterator();
            res.append(cubeIterator.next().toString());
            while (cubeIterator.hasNext()) {
                res.append(" OR ").append(cubeIterator.next().toString());
            }
        }
        return res.toString();
    }
}
