package Formulas.PropositionalFormulas.UnAryPropositionalFormulas;

import Formulas.Formula;
import Formulas.PropositionalFormulas.PropositionalFormula;

import java.util.HashSet;

public abstract class UnAryFormula extends PropositionalFormula {

    protected PropositionalFormula prop;

    @Override
    public HashSet<PropositionalFormula> getPropSet() {
        HashSet<PropositionalFormula> hspl = new HashSet<>();
        hspl.add(prop);
        return hspl;
    }

    @Override
    public HashSet<String> getVariables() { return prop.getVariables(); }
}
