package Formulas.PropositionalFormulas.BinAryPropositionalFormulas;

// import Formulas.Formula;
import Formulas.PropositionalFormulas.PropositionalFormula;

import java.util.HashSet;

public abstract class BinAryFormula extends PropositionalFormula {

    protected PropositionalFormula prop1;
    protected PropositionalFormula prop2;

    public BinAryFormula(PropositionalFormula p1, PropositionalFormula p2) {
        prop1 = p1;
        prop2 = p2;
    }

    public HashSet<PropositionalFormula> getPropSet() {
        HashSet<PropositionalFormula> hspf = new HashSet<>();
        hspf.add(prop1);
        hspf.add(prop2);
        return hspf;
    }

    @Override
    public HashSet<String> getVariables() {
        HashSet<String> bvfvars = new HashSet<>();
        for (String v: prop1.getVariables()) { bvfvars.add(v); }
        for (String v: prop2.getVariables()) { bvfvars.add(v); }
        return bvfvars;
    }

    protected String toString(String op) {
        StringBuilder res = new StringBuilder();
        res.append(prop1.toString());
        res.append(" ").append(op).append(" ");
        res.append(prop2.toString());
        return res.toString();
    }
}
