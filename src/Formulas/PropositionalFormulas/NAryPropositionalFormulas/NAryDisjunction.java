package Formulas.PropositionalFormulas.NAryPropositionalFormulas;

import Formulas.PropositionalFormulas.BinAryPropositionalFormulas.Disjunction;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas.Top;
import TruthTables.AssociationTable;

import java.util.Iterator;

public class NAryDisjunction extends NAryFormula {

    public NAryDisjunction() { super(); }

    @Override
    public PropositionalFormula toBinary() {
        // System.out.println("Appel de NAryDisjunction.toBinary()");
        PropositionalFormula res = new Top(); // Temporairement, pour qu'IntelliJ ne râle pas
        if (propSet.size() == 0) { // Ne devrait pas se produire
            res = new Top();
        } else if (propSet.size() == 1) { // Ne devrait pas se produire non plus
            for (PropositionalFormula p: propSet) { res = p; }
        }
        else {
            Iterator<PropositionalFormula> it = propSet.iterator();
            PropositionalFormula p1 = it.next();
            PropositionalFormula p2 = it.next();
            res = new Disjunction(p1, p2);
            while (it.hasNext()) {
                PropositionalFormula p = it.next();
                res = new Disjunction(res, p);
            }
        }
        // System.out.println("Résultat de la mise en forme binaire : "+res.toString());
        return res;
    }

    @Override
    public PropositionalFormula toNFNF() {
        NAryDisjunction pnnf = new NAryDisjunction();
        for (PropositionalFormula p : propSet) {
            pnnf.addProposition(p.toNFNF());
        }
        return pnnf;
    }

    @Override
    public PropositionalFormula toNFNFneg() {
        NAryConjunction pnnf = new NAryConjunction();
        for (PropositionalFormula p : propSet) {
            pnnf.addProposition(p.toNFNFneg());
        }
        return pnnf;
    }

    @Override
    public PropositionalDNFFormula toDNF() {
        return null;
    }

    @Override
    public PropositionalDNFFormula toDNFneg() {
        return null;
    }

    @Override
    public Boolean evaluateInitial(AssociationTable at) {
        Boolean rep = false;
        PropositionalFormula f;
        Iterator<PropositionalFormula> it = propSet.iterator();
        while (it.hasNext() && !rep) {
            f = it.next();
            rep = rep || f.evaluateInitial(at);
        }
        return rep;
    }

    @Override
    public Boolean evaluateFinal(AssociationTable at) {
        Boolean rep = false;
        PropositionalFormula f;
        Iterator<PropositionalFormula> it = propSet.iterator();
        while (it.hasNext() && !rep) {
            f = it.next();
            rep = rep || f.evaluateFinal(at);
        }
        return rep;
    }

    /* public VariationalFormula toNNFeqf() { return null; }

    public VariationalFormula toNNFeqt() {
        return null;
    }

    public VariationalFormula toNNFpls() {
        return null;
    }

    public VariationalFormula toNNFmns() {
        return null;
    } */

    @Override
    public String toString() {
        return super.toString("OR");
    }
}
