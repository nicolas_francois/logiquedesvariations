package Formulas;

import java.util.HashSet;

public abstract class Formula {

    /**
     * The set of variables composing the formula
     * @return a set of strings
     */
    public abstract HashSet<String> getVariables();


    /**
     * Conversion to String
     * @return a String representing the formula
     */
    public abstract String toString();
}
