package Formulas.VariationalFormulas.UnAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.VariationalFormula;
import TruthTables.AssociationTable;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public class NegationVar extends UnAryVarFormula {

    public NegationVar(VariationalFormula f) { super(f); }

    public VariationalFormula toNFNF() { return (VariationalFormula) prop.toNFNFneg(); }

    public VariationalFormula toNFNFneg() { return (VariationalFormula) prop; }

    @Override
    public Boolean evaluate(AssociationTable at) { return !prop.evaluate(at); }

    public DNFFormula toDNF() { return prop.toDNFneg(); }

    public DNFFormula toDNFneg() { return prop.toDNF(); }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("not (");
        res.append(prop.toString());
        res.append(")");
        return res.toString();
    }
}
