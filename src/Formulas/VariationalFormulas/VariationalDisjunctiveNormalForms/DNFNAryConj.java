package Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class DNFNAryConj extends DNFFormula {

    private ArrayList<DNFFormula> dnfFormList;

    public DNFNAryConj() {
        super();
        dnfFormList = new ArrayList<>();
    }

    public void addProposition(DNFFormula f) { dnfFormList.add(f); }

    public DNFFormula toDNF() {
        DNFFormula rep = new DNFFormula();
        if (dnfFormList.isEmpty()) { return rep; }
        CubesList temp = new CubesList();
        Iterator<DNFFormula> itf = dnfFormList.iterator();
        temp.addCubesList(itf.next().cubes);
        while (itf.hasNext()) {
            temp = temp.timesCubesList(itf.next().getCubes());
        }
        rep.addCubesList(temp);
        return rep;
    }
}
