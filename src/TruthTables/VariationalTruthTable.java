package TruthTables;

import Formulas.VariationalFormulas.VariationalFormula;

import static java.lang.Math.max;

/**
 * Created by Nicolas FRANCOIS on 29/04/2023.
 */

public class VariationalTruthTable {

    private VariationalFormula prop;
    private AssociationTable assocTable;
    private StringBuilder truthTable;

    public VariationalTruthTable(VariationalFormula f) {
        prop = f;
        assocTable = new AssociationTable(prop.getVariables());
        truthTable = new StringBuilder();
        // Printing start line
        truthTable.append("-");
        for (Association as : assocTable) {
            truthTable.append("-".repeat(max(4, 2 + as.getName().length())));
            truthTable.append("-");
        }
        truthTable.append("--------\n");
        // Printing labels
        truthTable.append("|");
        for (Association as : assocTable) {
            truthTable.append(" ");
            String name = as.getName();
            truthTable.append(name);
            if (name.length() < 2) {
                truthTable.append(" ");
            }
            truthTable.append(" |");
        }
        truthTable.append(" value |\n");
        // Printing separator line
        truthTable.append("|");
        for (Association as : assocTable) {
            truthTable.append("-".repeat(max(4, 2 + as.getName().length())));
            truthTable.append("|");
        }
        truthTable.append("-------|\n");
        // Printing each variable Symbol, and value of expression
        Boolean over = false;
        // DEBUG
        // int compteur = 0;
        // END DEBUG
        while (!over) {
            // DEBUG
            // compteur += 1;
            // System.out.println("Passage : "+compteur);
            // END DEBUG
            over = assocTable.endIter();
            truthTable.append("|");
            for (Association as : assocTable) {
                truthTable.append(" ");
                String tr;
                switch (as.getVs().getSymbol()) {
                    case EQUALSFALSE:
                        tr = "=f";
                        break;
                    case EQUALSTRUE:
                        tr = "=t";
                        break;
                    case PLUS:
                        tr = "+ ";
                        break;
                    default: // MINUS
                        tr = "- ";
                        break;
                }
                truthTable.append(tr);
                if (as.getName().length() > 2) {
                    truthTable.append(" ".repeat(as.getName().length() - 2));
                }
                truthTable.append(" |");
            }
            if (prop.evaluate(assocTable)) {
                truthTable.append(" true  |\n");
            } else {
                truthTable.append(" false |\n");
            }
            assocTable.iterAssoc();
        }
        // Printing end line
        truthTable.append("-");
        for (Association as : assocTable) {
            truthTable.append("-".repeat(max(4, 2 + as.getName().length())));
            truthTable.append("-");
        }
        truthTable.append("--------\n");
    }

    public String getTruthTable() {
        return truthTable.toString();
    }
}
