package Formulas.PropositionalFormulas.NAryPropositionalFormulas;

import Formulas.PropositionalFormulas.BinAryPropositionalFormulas.Conjunction;
import Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms.PropositionalDNFFormula;
import Formulas.PropositionalFormulas.PropositionalFormula;
import Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas.Bottom;
import TruthTables.AssociationTable;

import java.util.Iterator;

public class NAryConjunction extends NAryFormula {

    public NAryConjunction() { super(); }

    @Override
    public PropositionalFormula toBinary() {
        // System.out.println("Appel de NAryConjunction.toBinary()");
        PropositionalFormula res = new Bottom(); // Temporairement, pour qu'IntelliJ ne râle pas
        if (propSet.size() == 0) { // Ne devrait pas se produire
            res = new Bottom();
        } else if (propSet.size() == 1) { // Ne devrait pas se produire non plus
            for (PropositionalFormula p: propSet) { res = p; }
        }
        else {
            Iterator<PropositionalFormula> it = propSet.iterator();
            PropositionalFormula p1 = it.next();
            PropositionalFormula p2 = it.next();
            res = new Conjunction(p1, p2);
            while (it.hasNext()) {
                PropositionalFormula p = it.next();
                res = new Conjunction(res, p);
            }
        }
        // System.out.println("Résultat de la mise en forme binaire : "+res.toString());
        return res;
    }

    @Override
    public PropositionalFormula toNFNF() {
        NAryConjunction pnnf = new NAryConjunction();
        for (PropositionalFormula p : propSet) {
            pnnf.addProposition(p.toNFNF());
        }
        return pnnf;
    }

    @Override
    public PropositionalFormula toNFNFneg() {
        NAryDisjunction pnnf = new NAryDisjunction();
        for (PropositionalFormula p : propSet) {
            pnnf.addProposition(p.toNFNFneg());
        }
        return pnnf;
    }

    @Override
    public PropositionalDNFFormula toDNF() {
        return null;
    }

    @Override
    public PropositionalDNFFormula toDNFneg() {
        return null;
    }

    @Override
    public Boolean evaluateInitial(AssociationTable at) {
        Boolean rep = true;
        PropositionalFormula f;
        Iterator<PropositionalFormula> it = propSet.iterator();
        while (it.hasNext() && rep) {
            f = it.next();
            rep = rep && f.evaluateInitial(at);
        }
        return rep;
    }

    @Override
    public Boolean evaluateFinal(AssociationTable at) {
        Boolean rep = true;
        PropositionalFormula f;
        Iterator<PropositionalFormula> it = propSet.iterator();
        while (it.hasNext() && rep) {
            f = it.next();
            rep = rep && f.evaluateFinal(at);
        }
        return rep;
    }

    @Override
    public String toString() {
        return super.toString("AND");
    }
}
