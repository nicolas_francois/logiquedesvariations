package Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms;

import Formulas.VariationalFormulas.NAryVariationalFormulas.NAryConjunctionVar;
import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import Formulas.VariationalFormulas.VariationalFormula;
import Formulas.VariationalFormulas.ZeroAryVariationalFormulas.Atom;
import TruthTables.AssociationTable;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class Cube implements Iterable<VariableVariation> {

    private ArrayList<VariableVariation> vvSet;

    public Cube() { vvSet = new ArrayList<>(); }

    public void addVarVar(VariableVariation v) { vvSet.add(v); }

    public ArrayList<VariableVariation> getVarVarSet() { return vvSet; }

    public Iterator<VariableVariation> iterator() { return vvSet.iterator(); }

    public Cube timesVarVar(VariableVariation v) {
        Cube rep = new Cube();
        // If the list is empty, then it represents true
        Iterator<VariableVariation> itvv = vvSet.iterator();
        Boolean sameDetected = false;
        Boolean incompatibleDetected = false;
        // We look for each variation in the present cube
        while (itvv.hasNext() && !incompatibleDetected) {
            VariableVariation vv = itvv.next();
            // If a variation identical to v was already detected, we just copy the end of the cube
            if (sameDetected) { rep.addVarVar(vv); }
            // else if the present variation has the same variable...
            else if (vv.getVariable().equals(v.getVariable())) {
                // and the same "signs" before and after, we just copy the end of the cube
                if (vv.getSignBefore().equals(v.getSignBefore())
                        && vv.getSignAfter().equals(v.getSignAfter())) {
                    sameDetected = true;
                    rep.addVarVar(vv);
                }
                // else there is an incompatibility, we just return null (considered as equivalent to false)
                else {
                    incompatibleDetected = true;
                }
            }
        }
        if (incompatibleDetected) { return null; }
        else {
            // If no variation was equal to v, we add it at the end of the list
            if (!sameDetected) { rep.addVarVar(v); }
            return rep;
        }
    }

    public Cube timesCube(Cube c) {
        if (c == null) { return null; }
        Cube rep = new Cube();
        for (VariableVariation vv: vvSet) { rep.addVarVar(vv); }
        for (VariableVariation vv: c) {
            rep = rep.timesVarVar(vv);
            if (rep == null) { return null; }
        }
        return rep;
    }

    public VariationalFormula toFragment() {
        NAryConjunctionVar nacv = new NAryConjunctionVar();
        for (VariableVariation vv: vvSet) {
            if (vv.getSignBefore()) {
                if (vv.getSignAfter()) { nacv.addProposition(new Atom(vv.getVariable(), new VariationSymbol(Symbol.EQUALS))); }
                else { nacv.addProposition(new Atom(vv.getVariable(), new VariationSymbol(Symbol.MINUS))); }
            }
            else {
                if (vv.getSignAfter()) { nacv.addProposition(new Atom(vv.getVariable(), new VariationSymbol(Symbol.PLUS))); }
                else { nacv.addProposition(new Atom(vv.getVariable(), new VariationSymbol(Symbol.EQUALS))); }
            }
        }
        return nacv;
    }

    public Boolean evaluate(AssociationTable at) {
        Boolean rep = true;
        Iterator<VariableVariation> vvIterator = vvSet.iterator();
        while (rep && vvIterator.hasNext()) { rep = rep && vvIterator.next().evaluate(at); }
        return rep;
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        if (vvSet.size() == 0) { res.append("True"); }
        else {
            Iterator<VariableVariation> vvit = vvSet.iterator();
            res.append("(");
            res.append(vvit.next().toString());
            while (vvit.hasNext()) { res.append(" AND ").append(vvit.next().toString()); }
            res.append(")");
        }
        return res.toString();
    }
}
