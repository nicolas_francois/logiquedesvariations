package Formulas.VariationalFormulas.UnAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationalFormula;

import java.util.HashSet;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public abstract class UnAryVarFormula extends VariationalFormula {

    VariationalFormula prop;

    public UnAryVarFormula(VariationalFormula f) { prop = f; }

    @Override
    public HashSet<VariationalFormula> getPropSet() {
        HashSet<VariationalFormula> hspl = new HashSet<>();
        hspl.add(prop);
        return hspl;
    }

    public VariationalFormula getProp() { return prop; }

    @Override
    public HashSet<String> getVariables() { return prop.getVariables(); }

    // public DNFFormula toDNF() { return null; }

    // public DNFFormula toDNFneg() { return null; }
}
