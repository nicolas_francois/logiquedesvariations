package Formulas.VariationalFormulas.ZeroAryVariationalFormulas;

import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;
import TruthTables.AssociationTable;

import java.util.HashSet;

import static Formulas.VariationalFormulas.VariationSymbols.Symbol.EQUALSFALSE;
import static Formulas.VariationalFormulas.VariationSymbols.Symbol.EQUALSTRUE;

/**
 * Created by Nicolas FRANCOIS on 20/04/2023.
 */

public abstract class ConstantVar extends ZeroAryVarFormula {

    private Boolean value;

    public ConstantVar(VariationSymbol v, Boolean b) {
        super(v);
        value = b; }

    public HashSet<String> getVariables() { return new HashSet<>(); }

    public Boolean getValue() {
        if (value) { return (vs == new VariationSymbol(EQUALSTRUE)); }
        else { return (vs == new VariationSymbol(EQUALSFALSE)); }
    }

    public Boolean evaluate(AssociationTable a) { return this.getValue(); }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        if (value) { res.append("T"); } else { res.append("F"); }
        res.append(vs.toString());
        return res.toString();
    }
}
