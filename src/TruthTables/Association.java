package TruthTables;

import Formulas.VariationalFormulas.VariationSymbols.Symbol;
import Formulas.VariationalFormulas.VariationSymbols.VariationSymbol;

/**
 * Created by Nicolas FRANCOIS on 01/05/2023.
 */

public class Association {

    private String name;
    private VariationSymbol vs;

    public Association(String v) {
        name = v;
        vs = new VariationSymbol(Symbol.EQUALSFALSE);
    }

    public String getName() {
        return name;
    }

    public VariationSymbol getVs() { return vs; }

    public void iterSymbol() {
        switch (vs.getSymbol()) {
            case EQUALSFALSE:
                vs = new VariationSymbol(Symbol.EQUALSTRUE);
                break;
            case EQUALSTRUE:
                vs = new VariationSymbol(Symbol.PLUS);
                break;
            case PLUS:
                vs = new VariationSymbol(Symbol.MINUS);
                break;
            default: // MINUS
                vs = new VariationSymbol(Symbol.EQUALSFALSE);
                break;
        }
    }

    public Boolean carry() {
        return vs.getSymbol().equals(Symbol.MINUS);
    }
}
