package Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms;

import Formulas.PropositionalFormulas.ZeroAryPropositionalFormulas.Variable;

/**
 * Created by Nicolas FRANCOIS on 16/05/2023.
 */

public class PropositionalLitteral {

    private Variable var;
    private Boolean sig;

    public PropositionalLitteral(Variable v, Boolean s) { var = v; sig = s; }

    public Variable getVariable() { return var; }
    public Boolean getSign() { return sig; }
}
