package Formulas.PropositionalFormulas.PropositionalDisjunctiveNormalForms;

import Formulas.VariationalFormulas.VariationalDisjunctiveNormalForms.DNFFormula;
import Formulas.VariationalFormulas.VariationalFormula;

/**
 * Created by Nicolas FRANCOIS on 09/05/2023.
 */

public class PropositionalDNF {

    private VariationalFormula vf;

    private DNFFormula dnfvf;

    public PropositionalDNF(VariationalFormula f) {
        vf = f;
        dnfvf = vf.toDNF();
    }

    public DNFFormula getDNF() { return dnfvf; }
}
